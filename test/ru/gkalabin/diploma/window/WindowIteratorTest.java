package ru.gkalabin.diploma.window;

import junit.framework.TestCase;
import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.walsh.FWTTest;

import java.util.Iterator;

/**
 * @author grigory.kalabin@gmail.com
 */
public class WindowIteratorTest extends TestCase {
  public void testCountWithPadding() {
    int count = 0;
    Signal signal = new Signal(FWTTest.getSamples(18, 17d));

    Iterator<WalshWindowedSignal> iterator = signal.getIterator(new RectangularWindow(4, 1d));
    while (iterator.hasNext()) {
      iterator.next();
      count++;
    }
    assertEquals(5, count);
  }

  public void testCount() {
    int count = 0;
    Signal signal = new Signal(FWTTest.getSamples(16, 17d));

    Iterator<WalshWindowedSignal> iterator = signal.getIterator(new RectangularWindow(4, 1d));
    while (iterator.hasNext()) {
      iterator.next();
      count++;
    }
    assertEquals(4, count);
  }
}
