package ru.gkalabin.diploma.window;

import junit.framework.TestCase;
import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.walsh.FWTTest;

/**
 * @author grigory.kalabin@gmail.com
 */
public class RectangularWindowTest extends TestCase {

  public void testFrame() {
    int left = 0;
    int right = 100;
    int wLeft = 17;
    int wRight = 67;
    Window window = new RectangularWindow(wRight - wLeft, .25d);
    WindowFrame frame = window.getFrame(wLeft);
    for (int i = left; i < right; i++) {
      boolean inWindow = i >= wLeft && i < wRight;
      assertEquals("idx=" + i, inWindow ? 1d : 0d, frame.value(i));
    }
  }

  public void testApplyFrameToSignal() {
    int N = 4096;
    Signal signal = new Signal(FWTTest.getSamples(N, 11d));
    int wLeft = 1024;
    int wRight = 2048;
    Window window = new RectangularWindow(wRight - wLeft, .25d);
    WalshWindowedSignal windowedSignal = window.getFrame(wLeft).apply(signal);
    for (int i = 0; i < N; i++) {
      double sample = windowedSignal.get(i);
      boolean inWindow = i >= wLeft && i < wRight;
      assertEquals("idx=" + i, inWindow ? 11d : 0d, sample);
    }
  }
}
