package ru.gkalabin.diploma.walsh;

import com.google.common.base.Function;
import junit.framework.TestCase;

/**
 * @author grigory.kalabin@gmail.com
 */
public class WalshTest extends TestCase {

  public void test_rev_0() {
    assertEquals(0, Walsh.rev(0, 0));
  }

  public void test_rev_base_1() {
    assertEquals(1056, Walsh.rev(1, 1056));
  }

  public void test_rev_base_3() {
    assertEquals(0, Walsh.rev(3, 0));
    assertEquals(4, Walsh.rev(3, 1));
    assertEquals(2, Walsh.rev(3, 2));
    assertEquals(6, Walsh.rev(3, 3));
    assertEquals(1, Walsh.rev(3, 4));
    assertEquals(5, Walsh.rev(3, 5));
    assertEquals(3, Walsh.rev(3, 6));
    assertEquals(7, Walsh.rev(3, 7));
  }

  public void test_partialRev_base_3() {
    assertEquals(8, Walsh.rev(3, 8));
    assertEquals(12, Walsh.rev(3, 9));
    assertEquals(10, Walsh.rev(3, 10));
    assertEquals(14, Walsh.rev(3, 11));
    assertEquals(9, Walsh.rev(3, 12));
    assertEquals(13, Walsh.rev(3, 13));
    assertEquals(11, Walsh.rev(3, 14));
    assertEquals(15, Walsh.rev(3, 15));
  }

  public void test_revrev() {
    for (int i = 0; i < 1000; i++) {
      assertEquals(i, Walsh.rev(5, Walsh.rev(5, i)));
    }
  }

  public void test_N8_v0() {
    Walsh w = new Walsh(3);
    Function<Integer, Integer> v = w.v(0);
    assertEquals(1, (int) v.apply(0));
    assertEquals(1, (int) v.apply(1));
    assertEquals(1, (int) v.apply(2));
    assertEquals(1, (int) v.apply(3));
    assertEquals(1, (int) v.apply(4));
    assertEquals(1, (int) v.apply(5));
    assertEquals(1, (int) v.apply(6));
    assertEquals(1, (int) v.apply(7));
  }

  public void test_N8_v1() {
    Walsh w = new Walsh(3);
    Function<Integer, Integer> v = w.v(1);
    assertEquals(1, (int) v.apply(0));
    assertEquals(-1, (int) v.apply(1));
    assertEquals(1, (int) v.apply(2));
    assertEquals(-1, (int) v.apply(3));
    assertEquals(1, (int) v.apply(4));
    assertEquals(-1, (int) v.apply(5));
    assertEquals(1, (int) v.apply(6));
    assertEquals(-1, (int) v.apply(7));
  }

  public void test_N8_v2() {
    Walsh w = new Walsh(3);
    Function<Integer, Integer> v = w.v(2);
    assertEquals(1, (int) v.apply(0));
    assertEquals(1, (int) v.apply(1));
    assertEquals(-1, (int) v.apply(2));
    assertEquals(-1, (int) v.apply(3));
    assertEquals(1, (int) v.apply(4));
    assertEquals(1, (int) v.apply(5));
    assertEquals(-1, (int) v.apply(6));
    assertEquals(-1, (int) v.apply(7));
  }

  public void test_N8_v3() {
    Walsh w = new Walsh(3);
    Function<Integer, Integer> v = w.v(3);
    assertEquals(1, (int) v.apply(0));
    assertEquals(-1, (int) v.apply(1));
    assertEquals(-1, (int) v.apply(2));
    assertEquals(1, (int) v.apply(3));
    assertEquals(1, (int) v.apply(4));
    assertEquals(-1, (int) v.apply(5));
    assertEquals(-1, (int) v.apply(6));
    assertEquals(1, (int) v.apply(7));
  }

  public void test_N8_v4() {
    Walsh w = new Walsh(3);
    Function<Integer, Integer> v = w.v(4);
    assertEquals(1, (int) v.apply(0));
    assertEquals(1, (int) v.apply(1));
    assertEquals(1, (int) v.apply(2));
    assertEquals(1, (int) v.apply(3));
    assertEquals(-1, (int) v.apply(4));
    assertEquals(-1, (int) v.apply(5));
    assertEquals(-1, (int) v.apply(6));
    assertEquals(-1, (int) v.apply(7));
  }

  public void test_N8_v5() {
    Walsh w = new Walsh(3);
    Function<Integer, Integer> v = w.v(5);
    assertEquals(1, (int) v.apply(0));
    assertEquals(-1, (int) v.apply(1));
    assertEquals(1, (int) v.apply(2));
    assertEquals(-1, (int) v.apply(3));
    assertEquals(-1, (int) v.apply(4));
    assertEquals(1, (int) v.apply(5));
    assertEquals(-1, (int) v.apply(6));
    assertEquals(1, (int) v.apply(7));
  }

  public void test_N8_v6() {
    Walsh w = new Walsh(3);
    Function<Integer, Integer> v = w.v(6);
    assertEquals(1, (int) v.apply(0));
    assertEquals(1, (int) v.apply(1));
    assertEquals(-1, (int) v.apply(2));
    assertEquals(-1, (int) v.apply(3));
    assertEquals(-1, (int) v.apply(4));
    assertEquals(-1, (int) v.apply(5));
    assertEquals(1, (int) v.apply(6));
    assertEquals(1, (int) v.apply(7));
  }

  public void test_N8_v7() {
    Walsh w = new Walsh(3);
    Function<Integer, Integer> v = w.v(7);
    assertEquals(1, (int) v.apply(0));
    assertEquals(-1, (int) v.apply(1));
    assertEquals(-1, (int) v.apply(2));
    assertEquals(1, (int) v.apply(3));
    assertEquals(-1, (int) v.apply(4));
    assertEquals(1, (int) v.apply(5));
    assertEquals(1, (int) v.apply(6));
    assertEquals(-1, (int) v.apply(7));
  }
}
