package ru.gkalabin.diploma.walsh;

import junit.framework.TestCase;
import ru.gkalabin.diploma.Config;
import ru.gkalabin.diploma.util.Util;

import static org.junit.Assert.assertArrayEquals;

/**
 * @author grigory.kalabin@gmail.com
 */
public class FWTTest extends TestCase {
  public void testCmpFastAndSlow() {
    int length = 1011;
    double[] signal = Util.padArray(getSamples(length, 9d));
    int n = Util.padN(length);
    int s = Util.intLog2(n);

    long t0 = System.currentTimeMillis();
    double[] fast = FWT.fastWT(signal, n, s);
    long t1 = System.currentTimeMillis();
    double[] slow = FWT.slowWT(signal, n, s);
    long t2 = System.currentTimeMillis();
    System.out.println("Fast: " + (t1 - t0));
    System.out.println("Slow: " + (t2 - t1));
    assertArrayEquals(fast, slow, Config.DOUBLE_DELTA);
  }

  public void testDirectAndRev() {
    int length = 113589;
    double[] signal = Util.padArray(getSamples(length, 13d));
    int n = Util.padN(length);
    int s = Util.intLog2(n);

    long t0 = System.currentTimeMillis();
    double[] spectrum = FWT.fastWT(signal, n, s);
    long t1 = System.currentTimeMillis();
    double[] revSignal = FWT.fastRevWT(spectrum, n, s);
    long t2 = System.currentTimeMillis();
    System.out.println("WT: " + (t1 - t0));
    System.out.println("RevWt: " + (t2 - t1));
    assertArrayEquals(signal, revSignal, Config.DOUBLE_DELTA);
  }

  public static double[] getSamples(int length, double value) {
    double[] result = new double[length];
    for (int i = 0; i < length; i++) {
      result[i] = value;
    }
    return result;
  }
}
