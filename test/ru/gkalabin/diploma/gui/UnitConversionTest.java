package ru.gkalabin.diploma.gui;

import junit.framework.TestCase;

/**
 * @author grigory.kalabin@gmail.com
 */
public class UnitConversionTest extends TestCase {

  public void test_dB_2_Times() {
    assertEquals(0.1d, UnitConversion.dB2Times(-10));
    assertEquals(0.01d, UnitConversion.dB2Times(-20));
    assertEquals(10d, UnitConversion.dB2Times(10));
    assertEquals(100d, UnitConversion.dB2Times(20));
  }
}
