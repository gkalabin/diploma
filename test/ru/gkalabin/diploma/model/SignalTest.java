package ru.gkalabin.diploma.model;

import junit.framework.TestCase;
import ru.gkalabin.diploma.Config;
import ru.gkalabin.diploma.window.RectangularWindow;

import java.util.Iterator;

import static org.junit.Assert.assertArrayEquals;

/**
 * @author grigory.kalabin@gmail.com
 */
public class SignalTest extends TestCase {
  public void testGet() throws Exception {
    double[] samples = new double[]{1, 2, 3, 4};
    Signal sig = new Signal(samples);

    assertArrayEquals(new double[]{1}, sig.get(0, 1), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{1, 2}, sig.get(0, 2), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{1, 2, 3}, sig.get(0, 3), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{1, 2, 3, 4}, sig.get(0, 4), Config.DOUBLE_DELTA);

    assertArrayEquals(new double[]{2}, sig.get(1, 2), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{2, 3}, sig.get(1, 3), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{2, 3, 4}, sig.get(1, 4), Config.DOUBLE_DELTA);

    assertArrayEquals(new double[]{3}, sig.get(2, 3), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{3, 4}, sig.get(2, 4), Config.DOUBLE_DELTA);

    assertArrayEquals(new double[]{4}, sig.get(3, 4), Config.DOUBLE_DELTA);

    assertArrayEquals(new double[]{1, 2, 3, 4, 0}, sig.get(0, 5), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{2, 3, 4, 0}, sig.get(1, 5), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{3, 4, 0}, sig.get(2, 5), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{1, 2, 3, 4, 0, 0}, sig.get(0, 6), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{1, 2, 3, 4, 0, 0, 0}, sig.get(0, 7), Config.DOUBLE_DELTA);
  }

  public void testGetIteratorExceedingWindow() throws Exception {
    Signal signal = new Signal(new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    Iterator<WalshWindowedSignal> iterator = signal.getIterator(new RectangularWindow(4, 1d));
    assertArrayEquals(new double[]{1, 2, 3, 4}, iterator.next().getValue(), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{5, 6, 7, 8}, iterator.next().getValue(), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{9, 10, 0, 0}, iterator.next().getValue(), Config.DOUBLE_DELTA);
    assertFalse(iterator.hasNext());
  }

  public void testGetIterator() throws Exception {
    Signal signal = new Signal(new double[]{1, 2, 3, 4, 5, 6, 7, 8});
    Iterator<WalshWindowedSignal> iterator = signal.getIterator(new RectangularWindow(4, 1d));
    assertArrayEquals(new double[]{1, 2, 3, 4}, iterator.next().getValue(), Config.DOUBLE_DELTA);
    assertArrayEquals(new double[]{5, 6, 7, 8}, iterator.next().getValue(), Config.DOUBLE_DELTA);
    assertFalse(iterator.hasNext());
  }
}
