#!/bin/sh
files=$(echo "$1"|sed 's@/$@@g')

for f in ${files}/*.dat;
do
	echo "Replot ${f}"
	./plot.sh ${f}
done