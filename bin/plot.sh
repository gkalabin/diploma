#!/bin/sh
file="$1"
echo "Plotting file: ${file}"

if [ ${file: -4} != ".dat" ]; then
  echo "Unexpected file type. Exit now."
  exit 1
fi

graph_name=`basename ${file} .dat`
echo "Graph name: ${graph_name}"

out_file=${file%.*}.ps
echo "Out file: ${out_file}"

gnuplot <<EOF
    unset border
    set zeroaxis
    unset yzeroaxis
    unset key
    set term post
    set out "${out_file}"
    set title "${graph_name}"
    plot "${file}" with impulses
EOF