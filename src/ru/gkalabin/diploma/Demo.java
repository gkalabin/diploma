package ru.gkalabin.diploma;

import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.codec.wav.WavReader;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * @author grigory.kalabin@gmail.com
 */
public class Demo {
  public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
    WavReader reader = new WavReader(Config.INPUT_FILE);
    Signal signal = reader.getSignal();
  }
}
