package ru.gkalabin.diploma;

import com.google.common.collect.Lists;
import ru.gkalabin.diploma.filter.Filter;
import ru.gkalabin.diploma.filter.HighPassFilter;
import ru.gkalabin.diploma.filter.LowPassFilter;
import ru.gkalabin.diploma.filter.MediumPassFilter;
import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.model.WalshWindowedSpectrum;
import ru.gkalabin.diploma.codec.wav.WavReader;
import ru.gkalabin.diploma.codec.wav.WavWriter;
import ru.gkalabin.diploma.window.RectangularWindow;
import ru.gkalabin.diploma.window.Window;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Read, window walsh, revert and write
 *
 * @author grigory.kalabin@gmail.com
 */
public class FilterDemo {
  public static void main(String[] args) throws IOException, UnsupportedAudioFileException, InterruptedException {
    process(new File("./samples/vivaldi.wav"));
    process(new File("./samples/metallica.wav"));
    process(new File("./samples/chopin.wav"));
  }

  private static void process(File file) throws IOException, UnsupportedAudioFileException, InterruptedException {
    WavReader reader = new WavReader(file);
    reader.getSignal();
    String baseFileName = file.getName().substring(0, file.getName().length() - 4);

    Filter LpfHalf = new LowPassFilter(Config.WINDOW_LENGTH / 2);
    Filter LpfQuarter = new LowPassFilter(Config.WINDOW_LENGTH * 3 / 4);

    Filter HpfHalf = new HighPassFilter(Config.WINDOW_LENGTH / 2);
    Filter HpfQuarter = new HighPassFilter(Config.WINDOW_LENGTH / 4);

    Filter MpfHalf = new MediumPassFilter(Config.WINDOW_LENGTH / 4, Config.WINDOW_LENGTH * 3 / 4);
    Filter MpfQuarter = new MediumPassFilter(Config.WINDOW_LENGTH * 3 / 8, Config.WINDOW_LENGTH * 5 / 8);

    String zero = "0";
    String _1 = String.valueOf(Config.WINDOW_LENGTH - 1);
    String _1_2 = String.valueOf(Config.WINDOW_LENGTH / 2);
    String _1_4 = String.valueOf(Config.WINDOW_LENGTH / 4);
    String _3_4 = String.valueOf(Config.WINDOW_LENGTH * 3 / 4);
    String _3_8 = String.valueOf(Config.WINDOW_LENGTH * 3 / 8);
    String _5_8 = String.valueOf(Config.WINDOW_LENGTH * 5 / 8);

    System.out.println("Echo: " + processFilters(reader, Lists.<Filter>newArrayList(), baseFileName + "_echo") + " ms");
    System.out.println("LPF Half: " + processFilters(reader, Lists.newArrayList(LpfHalf), baseFileName +
        "_[" + zero + ":" + _1_2 + "]") + " ms");
    System.out.println("LPF Quarter: " + processFilters(reader, Lists.newArrayList(LpfQuarter), baseFileName +
        "_[" + zero + ":" + _3_4 + "]") + " ms");

    System.out.println("HPF Half: " + processFilters(reader, Lists.newArrayList(HpfHalf), baseFileName +
        "_[" + _1_2 + ":" + _1 + "]") + " ms");
    System.out.println("HPF Quarter: " + processFilters(reader, Lists.newArrayList(HpfQuarter), baseFileName +
        "_[" + _1_4 + ":" + _1 + "]") + " ms");

    System.out.println("MPF Half: " + processFilters(reader, Lists.newArrayList(MpfHalf), baseFileName +
        "_[" + zero + ":" + _1_4 + "]_[" + _3_4 + ":" + _1 + "]") + " ms");
    System.out.println("MPF Quarter: " + processFilters(reader, Lists.newArrayList(MpfQuarter), baseFileName +
        "_[" + zero + ":" + _3_8 + "]_[" + _5_8 + ":" + _1 + "]") + " ms");
  }

  private static long processFilters(WavReader reader, List<Filter> filters, String outFileName) throws IOException, UnsupportedAudioFileException, InterruptedException {
    long t0 = System.currentTimeMillis();
    Window window = new RectangularWindow(Config.WINDOW_LENGTH, Config.WINDOW_SHIFT_NO_OVERLAP);
    Iterator<WalshWindowedSignal> iterator = reader.getSignal().getIterator(window);
    List<WalshWindowedSignal> windowedSignals = Lists.newArrayList();

    while (iterator.hasNext()) {
      WalshWindowedSignal windowedSignal = iterator.next();

      WalshWindowedSpectrum spectrum = windowedSignal.toSpectrum();
      spectrum.applyFilters(filters);

      WalshWindowedSignal revertedSignal = spectrum.toSignal();
      windowedSignals.add(revertedSignal);
    }

    Signal revSignal = Signal.fromWindowedSignals(windowedSignals, reader.getSigLength());
    WavWriter writer = new WavWriter(new File("out/" + outFileName + ".wav"), reader);
    writer.write(revSignal);
    return System.currentTimeMillis() - t0;
  }
}
