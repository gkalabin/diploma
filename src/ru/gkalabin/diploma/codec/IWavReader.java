package ru.gkalabin.diploma.codec;

import javax.sound.sampled.AudioFormat;

public interface IWavReader {

  AudioFormat getFormat();

  long getFrameLength();

}
