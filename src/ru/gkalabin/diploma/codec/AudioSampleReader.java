package ru.gkalabin.diploma.codec;

import com.google.common.io.ByteStreams;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;

public class AudioSampleReader {

  private AudioInputStream audioInputStream;
  private AudioFormat format;

  public AudioSampleReader(File file) throws UnsupportedAudioFileException, IOException {
    audioInputStream = AudioSystem.getAudioInputStream(file);
    format = audioInputStream.getFormat();
  }

  // Return audio format, and through it, most properties of
  // the audio file: sample size, sample rate, etc.
  public AudioFormat getFormat() {
    return format;
  }

  public long getFrameLength() {
    return audioInputStream.getFrameLength();
  }

  // Return the number of samples of all channels
  public long getSampleCount() {
    long total = (audioInputStream.getFrameLength() * format.getFrameSize() * 8) / format.getSampleSizeInBits();
    return total / format.getChannels();
  }

  // Get the intervealed decoded samples for all channels, from sample
  // index begin (included) to sample index end (excluded) and copy
  // them into samples. end must not exceed getSampleCount(), and the
  // number of samples must not be so large that the associated byte
  // array cannot be allocated
  public double[] getInterleavedSamples() throws IOException,
      IllegalArgumentException {
    // allocate a byte buffer
    byte[] inBuffer = readBytesAndCloseStream();


    // decode bytes into samples. Supported encodings are:
    // PCM-SIGNED, PCM-UNSIGNED, A-LAW, U-LAW
    return decodeBytes(inBuffer);
  }

  protected AudioInputStream getAudioInputStream() {
    return audioInputStream;
  }

  protected byte[] readBytesAndCloseStream() throws IOException {
    byte[] result = ByteStreams.toByteArray(audioInputStream);
    audioInputStream.close();
    return result;
  }

  // Private. Decode bytes of audioBytes into audioSamples
  protected double[] decodeBytes(byte[] audioBytes) {
    int sampleSizeInBytes = format.getSampleSizeInBits() / 8;
    int[] sampleBytes = new int[sampleSizeInBytes];
    double[] audioSamples = new double[audioBytes.length / sampleSizeInBytes];
    int k = 0; // index in audioBytes
    for (int i = 0; i < audioSamples.length; i++) {
      // collect sample byte in big-endian order
      if (format.isBigEndian()) {
        // bytes start with MSB
        for (int j = 0; j < sampleSizeInBytes; j++) {
          sampleBytes[j] = audioBytes[k++];
        }
      } else {
        // bytes start with LSB
        for (int j = sampleSizeInBytes - 1; j >= 0; j--) {
          sampleBytes[j] = audioBytes[k++];
        }
      }
      // get integer value from bytes
      int ival = 0;
      for (int j = 0; j < sampleSizeInBytes; j++) {
        ival += sampleBytes[j];
        if (j < sampleSizeInBytes - 1) ival <<= 8;
      }
      // decode value
      double ratio = Math.pow(2d, format.getSampleSizeInBits() - 1d);
      double val = ((double) ival) / ratio;
      audioSamples[i] = val;
    }
    return audioSamples;
  }
}