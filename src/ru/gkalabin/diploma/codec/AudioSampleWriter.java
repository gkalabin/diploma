package ru.gkalabin.diploma.codec;

import com.google.common.io.Closer;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;


public class AudioSampleWriter {

  private File file;
  private AudioFormat format;
  private AudioFileFormat.Type targetType;

  public AudioSampleWriter(File file, AudioFormat format,
                           AudioFileFormat.Type targetType) throws IOException {
    this.format = format;
    this.targetType = targetType;
    this.file = file;
  }


  public void writeInterleavedSamples(double[] interleavedSamples) throws IOException {
    byte[] bytes = encodeSamples(interleavedSamples);

    Closer closer = Closer.create();
    try {
      ByteArrayInputStream inputStream = closer.register(new ByteArrayInputStream(bytes));
      AudioInputStream audioInputStream = closer.register(new AudioInputStream(inputStream, format, AudioSystem.NOT_SPECIFIED));
      AudioSystem.write(audioInputStream, targetType, file);
    } catch (Throwable t) {
      closer.rethrow(t);
    } finally {
      closer.close();
    }
  }

  protected byte[] encodeSamples(double[] audioData) throws IOException {
    byte[] audioBytes = new byte[audioData.length * (format.getSampleSizeInBits() / 8)];
    int in;
    if (format.getSampleSizeInBits() == 16) {
      if (format.isBigEndian()) {
        for (int i = 0; i < audioData.length; i++) {
          in = (int) (audioData[i] * 32768d);
                    /* First byte is MSB (high order) */
          audioBytes[2 * i] = (byte) (in >> 8);
                    /* Second byte is LSB (low order) */
          audioBytes[2 * i + 1] = (byte) (in & 255);
        }
      } else {
        for (int i = 0; i < audioData.length; i++) {
          in = (int) (audioData[i] * 32768d);
                    /* First byte is LSB (low order) */
          byte firstByte = (byte) (in & 255);
          audioBytes[2 * i] = firstByte;
                    /* Second byte is MSB (high order) */
          audioBytes[2 * i + 1] = (byte) ((in - firstByte) >> 8);
        }
      }
    } else if (format.getSampleSizeInBits() == 8) {
      if (format.getEncoding().toString().startsWith("PCM_SIGN")) {
        for (int i = 0; i < audioData.length; i++) {
          audioBytes[i] = (byte) (audioData[i] * 127);
        }
      } else {
        for (int i = 0; i < audioData.length; i++) {
          audioBytes[i] = (byte) (audioData[i] * 127 + 127);
        }
      }
    }
    return audioBytes;
  }
}