package ru.gkalabin.diploma.codec.wal;

import com.google.common.io.Closer;
import ru.gkalabin.diploma.codec.AudioSampleReader;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.zip.InflaterInputStream;

public class WalSampleReader extends AudioSampleReader {

  private int myWindowLength;
  private int mySigLength;

  public WalSampleReader(File file) throws UnsupportedAudioFileException, IOException {
    super(file);
  }

  @Override
  protected byte[] readBytesAndCloseStream() throws IOException {
    byte[] bytes = inflate(getAudioInputStream());
    ByteBuffer sigLengthBuffer = ByteBuffer.wrap(Arrays.copyOfRange(bytes, 0, 4));
    mySigLength = sigLengthBuffer.getInt();
    ByteBuffer windowLengthBuffer = ByteBuffer.wrap(Arrays.copyOfRange(bytes, 4, 8));
    myWindowLength = windowLengthBuffer.getInt();
    return Arrays.copyOfRange(bytes, 8, bytes.length);
  }

  @Override
  protected double[] decodeBytes(byte[] audioBytes) {
    int samplesCount = (int) (audioBytes.length / (WalSampleWriter.BYTES_PER_SAMPLE + 8d / myWindowLength));
    int windowCount = samplesCount / myWindowLength;
    int bytesPerWindow = 8 + myWindowLength * WalSampleWriter.BYTES_PER_SAMPLE;
    double[] audioSamples = new double[samplesCount];

    for (int windowIdx = 0; windowIdx < windowCount; windowIdx++) {
      int byteIdx = windowIdx * bytesPerWindow;
      double ratio = ByteBuffer.wrap(Arrays.copyOfRange(audioBytes, byteIdx, byteIdx + 8)).getDouble();
      byteIdx += 8;

      for (int sampleIdx = windowIdx * myWindowLength; sampleIdx < (windowIdx + 1) * myWindowLength; sampleIdx++) {
        byte[] sampleBytes = Arrays.copyOfRange(audioBytes, byteIdx, byteIdx + 2);
        byteIdx += 2;

        short ival = ByteBuffer.wrap(sampleBytes).getShort();
        // decode value
        double val = ((double) ival) / ratio;
        audioSamples[sampleIdx] = val;
      }
    }
    return audioSamples;
  }

  public int getWindowLength() {
    return myWindowLength;
  }

  public int getSigLength() {
    return mySigLength;
  }

  /**
   * Read content from provided stream, inflates it and returns byte array representing the content.
   * <br><strong>This method closes provided stream.</strong>
   *
   * @param contentStream containing the content. Will be closed.
   * @return inflated content as byte array
   * @throws java.io.IOException if any IO problems occurs
   */
  private static byte[] inflate(InputStream contentStream) throws IOException {
    Closer closer = Closer.create();
    try {
      BufferedInputStream bufferedStream = closer.register(new BufferedInputStream(closer.register(contentStream)));
      ObjectInputStream ois = closer.register(new ObjectInputStream(closer.register(new InflaterInputStream(bufferedStream))));
      return (byte[]) ois.readObject();
    } catch (Throwable e) {
      throw closer.rethrow(e);
    } finally {
      closer.close();
    }
  }
}