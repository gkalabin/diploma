package ru.gkalabin.diploma.codec.wal;

import com.google.common.base.Preconditions;
import ru.gkalabin.diploma.codec.wav.WavReader;
import ru.gkalabin.diploma.model.WalshWindowedSpectrum;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author grigory.kalabin@gmail.com
 */
public class WalWriter {

  private final File myFile;
  private final WavReader myReader;

  public WalWriter(File file, WavReader reader) {
    myFile = Preconditions.checkNotNull(file);
    myReader = Preconditions.checkNotNull(reader);
  }

  public void write(List<WalshWindowedSpectrum> spectrums) throws IOException, UnsupportedAudioFileException {
    int windowLength = -1;
    int samplesLength = 0;
    for (WalshWindowedSpectrum spectrum : spectrums) {
      samplesLength = Math.max(samplesLength, spectrum.getRight());
      if (windowLength == -1) {
        windowLength = spectrum.getRight() - spectrum.getLeft();
      } else {
        Preconditions.checkArgument(windowLength == spectrum.getRight() - spectrum.getLeft());
      }
    }

    WalSampleWriter audioWriter = new WalSampleWriter(myFile, myReader.getFormat(), AudioFileFormat.Type.WAVE,
        windowLength, myReader.getSigLength());
    double[] samples = new double[samplesLength];
    Arrays.fill(samples, 0d);
    for (WalshWindowedSpectrum spectrum : spectrums) {
      System.arraycopy(spectrum.getValue(), 0, samples, spectrum.getLeft(), spectrum.getRight() - spectrum.getLeft());
    }

    audioWriter.writeInterleavedSamples(samples);
  }
}
