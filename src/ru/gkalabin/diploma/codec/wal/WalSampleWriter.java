package ru.gkalabin.diploma.codec.wal;

import com.google.common.io.Closer;
import ru.gkalabin.diploma.codec.AudioSampleWriter;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;


public class WalSampleWriter extends AudioSampleWriter {

  private final int myWindowLength;
  private final int mySigLength;
  static final int BYTES_PER_SAMPLE = 2;

  public WalSampleWriter(File file, AudioFormat format,
                         AudioFileFormat.Type targetType, int windowLength, int sigLength) throws IOException {
    super(file, format, targetType);
    myWindowLength = windowLength;
    mySigLength = sigLength;
  }

  @Override
  protected byte[] encodeSamples(double[] audioData) throws IOException {
    // encode samples as usual
    byte[] audioBytes = encodeAudioSamples(audioData);

    // put window length
    ByteBuffer windowLengthConverter = ByteBuffer.allocate(4);
    windowLengthConverter.putInt(myWindowLength);

    // put src signal length
    ByteBuffer sigLengthConverter = ByteBuffer.allocate(4);
    sigLengthConverter.putInt(mySigLength);

    byte[] result = new byte[audioBytes.length + 8];
    System.arraycopy(sigLengthConverter.array(), 0, result, 0, 4);
    System.arraycopy(windowLengthConverter.array(), 0, result, 4, 4);
    System.arraycopy(audioBytes, 0, result, 8, audioBytes.length);

    // then deflate it
    return deflate(result);
  }

  private byte[] encodeAudioSamples(double[] audioData) throws IOException {
    int windowCount = audioData.length / myWindowLength;
    byte[] audioBytes = new byte[audioData.length * BYTES_PER_SAMPLE + windowCount * 8];

    int byteIdx = 0;
    for (int i = 0; i < windowCount; i++) {
      int winStart = i * myWindowLength;
      int winEnd = (i + 1) * myWindowLength;

      double max = -1;
      for (int j = winStart; j < winEnd; j++) {
        double sample = Math.abs(audioData[j]);
        if (max < sample) {
          max = sample;
        }
      }

      double ratio = Short.MAX_VALUE / max;
      byte[] ratioBytes = ByteBuffer.allocate(8).putDouble(ratio).array();
      System.arraycopy(ratioBytes, 0, audioBytes, byteIdx, 8);
      byteIdx += 8;

      for (int j = winStart; j < winEnd; j++) {
        short in = (short) Math.round(audioData[j] * ratio);
        byte[] bytes = ByteBuffer.allocate(2).putShort(in).array();
        System.arraycopy(bytes, 0, audioBytes, byteIdx, 2);
        byteIdx += 2;
      }
    }
    return audioBytes;
  }

  private static byte[] deflate(byte[] content) throws IOException {
    Deflater deflater = new Deflater(Deflater.BEST_SPEED);
    Closer closer = Closer.create();
    try {
      ByteArrayOutputStream buffer = closer.register(new ByteArrayOutputStream());
      DeflaterOutputStream dos = closer.register(new DeflaterOutputStream(buffer, deflater));
      ObjectOutputStream oos = closer.register(new ObjectOutputStream(dos));
      oos.writeObject(content);
      oos.flush();
      oos.close();
      return buffer.toByteArray();
    } catch (Throwable e) {
      throw closer.rethrow(e);
    } finally {
      closer.close();
    }
  }
}