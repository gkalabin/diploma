package ru.gkalabin.diploma.codec.wal;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import ru.gkalabin.diploma.codec.IWavReader;
import ru.gkalabin.diploma.model.WalshWindowedSpectrum;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class WalReader implements IWavReader {
  private WalSampleReader myReader;
  private List<WalshWindowedSpectrum> mySpectrum;

  public WalReader(File file) {
    try {
      myReader = new WalSampleReader(Preconditions.checkNotNull(file));
    } catch (UnsupportedAudioFileException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public List<WalshWindowedSpectrum> getSpectrum() throws IOException, UnsupportedAudioFileException {
    if (mySpectrum != null) {
      return mySpectrum;
    }

    double[] rawSamples = myReader.getInterleavedSamples();
    int windowLength = myReader.getWindowLength();

    mySpectrum = Lists.newArrayList();
    int idx = 0;
    while (idx + windowLength <= rawSamples.length) {
      double[] windowedSamples = new double[windowLength];
      System.arraycopy(rawSamples, idx, windowedSamples, 0, windowLength);
      mySpectrum.add(new WalshWindowedSpectrum(windowedSamples, idx, idx + windowLength));
      idx += windowLength;
    }

    return mySpectrum;
  }

  public int getSigLength() {
    return myReader.getSigLength();
  }

  public AudioFormat getFormat() {
    return myReader.getFormat();
  }

  public long getFrameLength() {
    return myReader.getFrameLength();
  }

}
