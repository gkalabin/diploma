package ru.gkalabin.diploma.codec.wav;

import com.google.common.base.Preconditions;
import ru.gkalabin.diploma.codec.AudioSampleReader;
import ru.gkalabin.diploma.codec.IWavReader;
import ru.gkalabin.diploma.model.Signal;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;

public class WavReader implements IWavReader {
  private AudioSampleReader myReader;
  private Signal mySignal;

  public WavReader(File file) {
    this(file, false);
  }

  public WavReader(File file, boolean compressed) {
    try {
      myReader = new AudioSampleReader(Preconditions.checkNotNull(file));
    } catch (UnsupportedAudioFileException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public Signal getSignal() throws IOException, UnsupportedAudioFileException {
    if (mySignal != null) {
      return mySignal;
    }

    double[] rawSamples = myReader.getInterleavedSamples();
    mySignal = new Signal(rawSamples);
    return mySignal;
  }

  public int getSigLength() throws IOException, UnsupportedAudioFileException {
    return getSignal().getLength();
  }

  public AudioFormat getFormat() {
    return myReader.getFormat();
  }

  public long getFrameLength() {
    return myReader.getFrameLength();
  }

}
