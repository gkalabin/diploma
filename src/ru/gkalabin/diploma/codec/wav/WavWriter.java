package ru.gkalabin.diploma.codec.wav;

import com.google.common.base.Preconditions;
import ru.gkalabin.diploma.codec.AudioSampleWriter;
import ru.gkalabin.diploma.codec.IWavReader;
import ru.gkalabin.diploma.model.Signal;

import javax.sound.sampled.AudioFileFormat;
import java.io.File;
import java.io.IOException;

/**
 * @author grigory.kalabin@gmail.com
 */
public class WavWriter {

  private final File myFile;
  private final IWavReader myReader;

  public WavWriter(File file, IWavReader reader) {
    myFile = Preconditions.checkNotNull(file);
    myReader = Preconditions.checkNotNull(reader);
  }

  public void write(Signal signal) throws IOException {
    AudioSampleWriter audioWriter = new AudioSampleWriter(myFile, myReader.getFormat(), AudioFileFormat.Type.WAVE);
    double[] samples = signal.getValue();
    audioWriter.writeInterleavedSamples(samples);
  }
}
