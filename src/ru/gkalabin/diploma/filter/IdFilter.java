package ru.gkalabin.diploma.filter;

/**
 * @author grigory.kalabin@gmail.com
 */
public class IdFilter implements Filter {
  @Override
  public double apply(int frequency, double magnitude) {
    return magnitude;
  }
}
