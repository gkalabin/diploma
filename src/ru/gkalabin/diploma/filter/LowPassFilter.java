package ru.gkalabin.diploma.filter;

/**
 * @author grigory.kalabin@gmail.com
 */
public class LowPassFilter implements Filter {
  private final int myFrequencyEdge;

  public LowPassFilter(int frequencyEdge) {
    myFrequencyEdge = frequencyEdge;
  }

  @Override
  public double apply(int frequency, double magnitude) {
    return frequency < myFrequencyEdge ? magnitude : 0;
  }
}
