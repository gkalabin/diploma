package ru.gkalabin.diploma.filter;

/**
 * @author grigory.kalabin@gmail.com
 */
public class MediumPassFilter implements Filter {
  private final int myMinFrequency;
  private final int myMaxFrequency;
  private final double myCoefficient;

  public MediumPassFilter(int minFrequency, int maxFrequency, double coefficient) {
    myMinFrequency = minFrequency;
    myMaxFrequency = maxFrequency;
    myCoefficient = coefficient;
  }

  public MediumPassFilter(int minFrequency, int maxFrequency) {
    this(minFrequency, maxFrequency, 0);
  }

  @Override
  public double apply(int frequency, double magnitude) {
    return frequency < myMinFrequency || frequency >= myMaxFrequency ? magnitude : magnitude * myCoefficient;
  }

  @Override
  public String toString() {
    return String.format(
        "frequency < %d || frequency >= %d ? magnitude : magnitude * %.2f",
        myMinFrequency, myMaxFrequency, myCoefficient);
  }
}
