package ru.gkalabin.diploma.filter;

/**
 * @author grigory.kalabin@gmail.com
 */
public interface Filter {
  /**
   * @return new magnitude
   */
  double apply(int frequency, double magnitude);
}
