package ru.gkalabin.diploma;

import java.io.File;

/**
 * @author grigory.kalabin@gmail.com
 */
public class Config {
  public static final boolean DEBUG = true;

  public static final File INPUT_FILE = new File("./samples/vivaldi.wav");
  public static final File OUTPUT_FILE = new File("./out/echo.wav");
  public static final File OUT_DIR = new File("./out/");

  public static final File GRAPH_DIR = new File("./graph/");
  public static final String DAT_FILE_FORMAT = "%s.dat";

  public static final double DOUBLE_DELTA = .0000001d;

  public static final int WINDOW_LENGTH = 4096;
  public static final double WINDOW_SHIFT_NO_OVERLAP = 1d;
}
