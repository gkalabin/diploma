package ru.gkalabin.diploma;

import ru.gkalabin.diploma.util.DoubleArrayDiff;
import ru.gkalabin.diploma.codec.wav.WavReader;
import ru.gkalabin.diploma.codec.wav.WavWriter;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;

/**
 * Read and write
 *
 * @author grigory.kalabin@gmail.com
 */
public class IdentityTest {

  public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
    double[] samples;
    File srcFile = Config.INPUT_FILE;
    File outFile = new File(Config.OUT_DIR, srcFile.getName());
    File outFile2 = new File(Config.OUT_DIR, srcFile.getName() + "2");
    {
      WavReader reader = new WavReader(srcFile);
      WavWriter writer = new WavWriter(outFile, reader);
      samples = reader.getSignal().getValue();
      writer.write(reader.getSignal());
    }
    {
      WavReader reader = new WavReader(outFile);
      WavWriter writer = new WavWriter(outFile2, reader);
      double[] samples2 = reader.getSignal().getValue();
      System.out.println(DoubleArrayDiff.getDiff(samples2, samples));
      writer.write(reader.getSignal());
    }
  }
}
