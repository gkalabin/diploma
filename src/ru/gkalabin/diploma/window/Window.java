package ru.gkalabin.diploma.window;

import java.util.Iterator;

/**
 * @author grigory.kalabin@gmail.com
 */
public abstract class Window {

  private final int myWindowLength;
  private final double myWindowShift;

  protected Window(int windowLength, double windowShift) {
    myWindowLength = windowLength;
    myWindowShift = windowShift;
  }

  public Iterator<WindowFrame> getFrameIterator(int from, int to) {
    return new WindowIterator(this, from, to);
  }

  public Iterator<WindowFrame> getFrameIterator(int to) {
    return new WindowIterator(this, to);
  }

  public int getLength() {
    return myWindowLength;
  }

  public double getShift() {
    return myWindowShift;
  }

  public abstract WindowFrame getFrame(int left);
}
