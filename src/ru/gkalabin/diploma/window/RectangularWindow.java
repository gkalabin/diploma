package ru.gkalabin.diploma.window;

/**
 * @author grigory.kalabin@gmail.com
 */
public class RectangularWindow extends Window {

  public RectangularWindow(int windowLength, double windowShift) {
    super(windowLength, windowShift);
  }

  @Override
  public WindowFrame getFrame(int left) {
    return new RectangularWindowFrame(left);
  }

  private class RectangularWindowFrame extends WindowFrame {

    private RectangularWindowFrame(int left) {
      super(left, left + getLength());
    }

    @Override
    public double value(int t) {
      return inWindow(t) ? 1d : 0d;
    }
  }
}
