package ru.gkalabin.diploma.window;

/**
 * @author grigory.kalabin@gmail.com
 */
public class HammingWindow extends Window {
  public HammingWindow(int windowLength, double windowShift) {
    super(windowLength, windowShift);
  }

  @Override
  public WindowFrame getFrame(int left) {
    return new HammingFrame(left);
  }

  private class HammingFrame extends WindowFrame {
    private double myCosArg;
    private double myArgShift;

    private HammingFrame(int left) {
      super(left, left + getLength());
      myArgShift = (getLength() - 1d) / 2d + left;
      myCosArg = 2 * Math.PI / getLength();
    }

    @Override
    public double value(int t) {
      if (!inWindow(t)) {
        return 0d;
      }
      double shiftedT = t - myArgShift;
      return .54d + .46d * Math.cos(shiftedT * myCosArg);
    }
  }
}
