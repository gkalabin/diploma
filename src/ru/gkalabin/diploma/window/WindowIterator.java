package ru.gkalabin.diploma.window;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author grigory.kalabin@gmail.com
 */
class WindowIterator implements Iterator<WindowFrame> {
  private final Window myWindow;
  private final int myFrom;
  private final int myTo;
  private int myCurrentFrameStart;

  WindowIterator(Window window, int from, int to) {
    myWindow = window;
    myFrom = from;
    myTo = to;
    myCurrentFrameStart = myFrom;
  }

  WindowIterator(Window window, int to) {
    this(window, 0, to);
  }

  @Override
  public boolean hasNext() {
    return myCurrentFrameStart < myTo;
  }

  @Override
  public WindowFrame next() {
    if (!hasNext()) {
      throw new NoSuchElementException();
    }
    WindowFrame frame = myWindow.getFrame(myCurrentFrameStart);
    myCurrentFrameStart += myWindow.getLength() * myWindow.getShift();
    return frame;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}