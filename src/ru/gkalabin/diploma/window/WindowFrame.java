package ru.gkalabin.diploma.window;

import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;

/**
 * @author grigory.kalabin@gmail.com
 */
public abstract class WindowFrame {
  private final int myLeft;
  private final int myRight;
  private final int myLength;

  protected WindowFrame(int left, int right) {
    myLeft = left;
    myRight = right;
    myLength = right - left;
  }

  public WalshWindowedSignal apply(Signal src) {
    double[] samples = src.get(myLeft, myRight);
    double[] windowedSamples = applyToSamples(samples);
    return new WalshWindowedSignal(windowedSamples, myLeft, myRight);
  }

  private double[] applyToSamples(double[] samples) {
    double[] windowedSamples = new double[myLength];
    for (int i = 0; i < myLength; i++) {
      windowedSamples[i] = samples[i] * value(i + myLeft);
    }
    return windowedSamples;
  }

  /**
   * string with window values from o..N
   */
  public String toString(int N) {
    StringBuilder result = new StringBuilder();
    result.append(String.format("l=%d, r=%d -> ", myLeft, myRight));
    for (int i = 0; i < N; i++) {
      result.append(String.format("%.2f    ", value(i)));
    }
    return result.toString();
  }

  protected int getLeft() {
    return myLeft;
  }

  protected int getRight() {
    return myRight;
  }

  protected boolean inWindow(int t) {
    return t >= getLeft() && t < getRight();
  }

  public abstract double value(int t);
}
