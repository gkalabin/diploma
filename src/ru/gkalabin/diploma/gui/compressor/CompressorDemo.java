package ru.gkalabin.diploma.gui.compressor;

import com.google.common.base.Strings;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ru.gkalabin.diploma.Config;
import ru.gkalabin.diploma.gui.PlayFileHandler;

import java.io.File;
import java.io.IOException;

/**
 * @author grigory.kalabin@gmail.com
 */
public class CompressorDemo extends Application {

  private static final int PADDING = 10;

  private TextField myInputFileField;
  private TextField myOutputFileField;

  private ToggleGroup myWindowLengthControl;
  private int myWindowLength;
  private boolean myRunCompression = true;


  private final TextArea myLogTextArea = new TextArea();
  private final Button myRunButton = new Button("Start");
  private final Button myPlayResultButton = new Button("Play result");
  private final ToggleGroup myActionKindControl = new ToggleGroup();

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(final Stage primaryStage) {
    primaryStage.setTitle("Short-time Walsh transform compression demo");
    Text title = new Text("Short-time Walsh transform compression demo");
    title.setFont(Font.font("Tahoma", FontWeight.NORMAL, 32));

    Text ioTitle = new Text("I/O");
    title.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

    GridPane ioGrid = new GridPane();
    ioGrid.setHgap(PADDING);
    ioGrid.setVgap(PADDING);
    ioGrid.setAlignment(Pos.CENTER);
    ColumnConstraints labelsColumn = new ColumnConstraints();
    labelsColumn.setHalignment(HPos.RIGHT);
    ColumnConstraints inputFieldsColumn = new ColumnConstraints();
    inputFieldsColumn.setHgrow(Priority.ALWAYS);
    ioGrid.getColumnConstraints().addAll(labelsColumn, inputFieldsColumn);

    myInputFileField = new TextField();
    Button pickInputFile = new Button("Pick");
    pickInputFile.setOnAction(new EventHandler<ActionEvent>() {
      private File myInitialDir = new File(".");

      @Override
      public void handle(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(myInitialDir);
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Wav and wal files (*.wav, *.wal)", "*.wav", "*.wal");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showOpenDialog(primaryStage);
        if (file != null) {
          myInitialDir = file.getParentFile();
          myInputFileField.setText(file.getAbsolutePath());
          setupOutputFile();
        }
      }
    });
    Button playInputFile = new Button("Play");
    playInputFile.setOnAction(new PlayFileHandler(myInputFileField, playInputFile));
    ioGrid.add(new Label("Input file:"), 0, 0);
    ioGrid.add(myInputFileField, 1, 0);
    ioGrid.add(pickInputFile, 2, 0);
    ioGrid.add(playInputFile, 3, 0);

    myOutputFileField = new TextField();
    ioGrid.add(new Label("Output file:"), 0, 1);
    ioGrid.add(myOutputFileField, 1, 1);
    Button out2inButton = new Button("To input");
    out2inButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        myInputFileField.setText(myOutputFileField.getText());
        setupOutputFile();
      }
    });
    ioGrid.add(out2inButton, 2, 1);
    ioGrid.add(new Label(), 3, 1);

    final HBox windowLengthBox = new HBox();
    myWindowLengthControl = new ToggleGroup();
    EventHandler<ActionEvent> windowLengthChangeHandler = new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        myWindowLength = (int) myWindowLengthControl.getSelectedToggle().getUserData();
      }
    };
    windowLengthBox.getChildren().addAll(
        new Label("Window length:    "),
        getWindowLengthRadioButton(128, windowLengthChangeHandler),
        getWindowLengthRadioButton(256, windowLengthChangeHandler),
        getWindowLengthRadioButton(512, windowLengthChangeHandler),
        getWindowLengthRadioButton(1024, windowLengthChangeHandler),
        getWindowLengthRadioButton(2048, windowLengthChangeHandler),
        getWindowLengthRadioButton(4096, windowLengthChangeHandler),
        getWindowLengthRadioButton(8192, windowLengthChangeHandler)
    );

    HBox actionKindBox = new HBox();
    EventHandler<ActionEvent> actionKindChangeListener = new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        myRunCompression = (boolean) myActionKindControl.getSelectedToggle().getUserData();
        setupOutputFile();
        windowLengthBox.setDisable(!myRunCompression);
      }
    };
    actionKindBox.getChildren().addAll(
        new Label("Action:  "),
        RadioButtonBuilder.create()
            .text("Compress (wav -> wal)" + Strings.repeat(" ", 10))
            .userData(true)
            .toggleGroup(myActionKindControl)
            .onAction(actionKindChangeListener)
            .selected(true)
            .build(),
        RadioButtonBuilder.create()
            .text("Uncompress (wal -> wav)")
            .userData(false)
            .toggleGroup(myActionKindControl)
            .onAction(actionKindChangeListener)
            .build()
    );


    HBox controlsBox = new HBox();
    controlsBox.setAlignment(Pos.CENTER_RIGHT);
    controlsBox.setSpacing(PADDING);
    myRunButton.setPrefSize(100, 40);
    myRunButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        myRunButton.setDisable(true);
        myPlayResultButton.setDisable(true);
//        myLogTextArea.clear();

        CompressorDemoRunner runner = new CompressorDemoRunner(
            myInputFileField.getText(), myOutputFileField.getText(), myWindowLength, myRunCompression,
            new CompressorDemoRunner.Logger() {
              private final StringBuilder myBuffer = new StringBuilder();

              @Override
              public void log(final String msg) {
                myBuffer.append(msg);
                myBuffer.append("\n\r");
              }

              @Override
              public void flush() {
                Platform.runLater(new Runnable() {
                  public void run() {
                    myLogTextArea.appendText(myBuffer.toString());
                  }
                });
              }
            }
        );
        new Thread(runner).start();

        myRunButton.setDisable(false);
        myPlayResultButton.setDisable(false);
      }
    });
    myPlayResultButton.setPrefSize(100, 40);
    myPlayResultButton.setDisable(true);
    myPlayResultButton.setOnAction(new PlayFileHandler(myOutputFileField, myPlayResultButton));
    controlsBox.getChildren().addAll(myRunButton, myPlayResultButton);

    myLogTextArea.setDisable(false);
    myLogTextArea.setEditable(false);
//    myLogTextArea.setPrefRowCount(Integer.MAX_VALUE);

    setupDefaults();
    StackPane root = new StackPane();
    VBox vBox = new VBox(PADDING);
    vBox.setPadding(new Insets(PADDING));
    VBox.setVgrow(myLogTextArea, Priority.ALWAYS);
    vBox.getChildren().addAll(
        title,
        new Separator(),
        ioTitle, ioGrid,
        new Separator(),
        windowLengthBox,
        new Separator(),
        actionKindBox,
        new Separator(),
        myLogTextArea,
        controlsBox
    );
    root.getChildren().addAll(vBox);

    primaryStage.setScene(new Scene(root));
    primaryStage.show();
  }

  private void setupDefaults() {
    for (Toggle t : myWindowLengthControl.getToggles()) {
      if (t.getUserData().equals(Config.WINDOW_LENGTH)) {
        t.setSelected(true);
        myWindowLength = Config.WINDOW_LENGTH;
      }
    }
    if (myWindowLengthControl.getSelectedToggle() == null) {
      throw new RuntimeException("Config not valid!");
    }

    try {
      myInputFileField.setText(Config.INPUT_FILE.getCanonicalPath());
      myOutputFileField.setText(new File(Config.OUT_DIR, Config.INPUT_FILE.getName()).getCanonicalPath());
    } catch (IOException e) {
      // TODO: log!
      e.printStackTrace();
    }

    setupOutputFile();
  }

  private void setupOutputFile() {
    File inFile = new File(myInputFileField.getText());
    String inFileBaseName = inFile.getName().substring(0, inFile.getName().length() - 4);
    String outFileName = inFileBaseName + (myRunCompression ? "_compressed.wal" : "_uncompressed.wav");
    File outFile = new File(Config.OUT_DIR, outFileName);
    try {
      myOutputFileField.setText(outFile.getCanonicalPath());
    } catch (IOException e) {
      // should never happen
    }
  }

  private RadioButton getWindowLengthRadioButton(int value, EventHandler<ActionEvent> windowLengthChangeHandler) {
    return RadioButtonBuilder.create()
        .text(value + Strings.repeat(" ", 10))
        .userData(value)
        .onAction(windowLengthChangeHandler)
        .toggleGroup(myWindowLengthControl)
        .build();
  }
}
