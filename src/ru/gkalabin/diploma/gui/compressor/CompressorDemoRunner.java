package ru.gkalabin.diploma.gui.compressor;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import javafx.concurrent.Task;
import ru.gkalabin.diploma.Config;
import ru.gkalabin.diploma.codec.wal.WalReader;
import ru.gkalabin.diploma.codec.wal.WalWriter;
import ru.gkalabin.diploma.codec.wav.WavReader;
import ru.gkalabin.diploma.codec.wav.WavWriter;
import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.model.WalshWindowedSpectrum;
import ru.gkalabin.diploma.window.RectangularWindow;
import ru.gkalabin.diploma.window.Window;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * @author grigory.kalabin@gmail.com
 */
public class CompressorDemoRunner extends Task {
  public interface Logger {
    void log(String msg);

    void flush();
  }

  private final int myWindowLength;
  private final boolean myCompress;
  private final Logger myLogger;
  private final File myInputFile;
  private final File myOutputFile;

  public CompressorDemoRunner(String inputFile, String outFile, int windowLength, boolean compress, Logger logger) {
    myWindowLength = windowLength;
    myCompress = compress;
    myLogger = logger;
    myInputFile = new File(inputFile);
    myOutputFile = new File(outFile);
  }

  @Override
  public Void call() {
    try {
      if (myCompress) {
        doCompress();
      } else {
        doUncompress();
      }
      long inputLength = myInputFile.length();
      long outputLength = myOutputFile.length();
      log("Input length is %d", inputLength);
      log("Output length is %d", outputLength);
      log("Ratio is %.3f", (double) inputLength / (double) outputLength);
      logSeparator();
    } catch (Exception e) {
      myLogger.log("Unhandled exception: " + e.getMessage());
      e.printStackTrace();
    }
    myLogger.flush();
    return null;
  }

  private void doUncompress() throws IOException, UnsupportedAudioFileException {
    log("Start decompression of %s", myInputFile.getCanonicalPath());
    long t0 = System.currentTimeMillis();
    WalReader reader = new WalReader(myInputFile);
    List<WalshWindowedSpectrum> spectrums = reader.getSpectrum();
    long t1 = System.currentTimeMillis();
    log("Signal reading took %d ms", t1 - t0);

    List<WalshWindowedSignal> signals = Lists.newArrayList();
    for (WalshWindowedSpectrum spectrum : spectrums) {
      signals.add(spectrum.toSignal());
    }
    Signal signal = Signal.fromWindowedSignals(signals, reader.getSigLength());
    long t2 = System.currentTimeMillis();
    log("Conversion took %d ms. Got signal with length = %d", t2 - t0, signal.getLength());

    WavWriter writer = new WavWriter(myOutputFile, reader);
    writer.write(signal);
    long t3 = System.currentTimeMillis();
    log("Whole processing time is %d ms", t3 - t0);
  }

  private void doCompress() throws IOException, UnsupportedAudioFileException {
    log("Start compression of %s", myInputFile.getCanonicalPath());
    long t0 = System.currentTimeMillis();
    WavReader reader = new WavReader(myInputFile);
    Signal signal = reader.getSignal();
    long t1 = System.currentTimeMillis();
    log("Signal (length=%d) reading took %d ms", signal.getLength(), t1 - t0);

    Window window = new RectangularWindow(myWindowLength, Config.WINDOW_SHIFT_NO_OVERLAP);
    Iterator<WalshWindowedSignal> iterator = signal.getIterator(window);
    List<WalshWindowedSpectrum> spectrums = Lists.newArrayList();
    while (iterator.hasNext()) {
      spectrums.add(iterator.next().toSpectrum());
    }
    long t2 = System.currentTimeMillis();
    log("Conversion took %d ms", t2 - t1);

    WalWriter writer = new WalWriter(myOutputFile, reader);
    writer.write(spectrums);
    long t3 = System.currentTimeMillis();
    log("Writing took %d ms", t3 - t2);
    log("Whole processing time is %d ms", t3 - t0);
  }


  private void logSeparator() {
    myLogger.log(Strings.repeat("-", 40));
  }

  private void log(String msg) {
    myLogger.log(msg);
  }

  private void log(String msgFormat, Object... args) {
    myLogger.log(String.format(msgFormat, args));
  }
}
