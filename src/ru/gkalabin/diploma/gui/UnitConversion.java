package ru.gkalabin.diploma.gui;

/**
 * @author grigory.kalabin@gmail.com
 */
public class UnitConversion {
  private UnitConversion() {
  }

  public static double dB2Times(double dB) {
    return Math.pow(10d, dB / 10);
  }
}
