package ru.gkalabin.diploma.gui.equalizer;

import com.google.common.base.Strings;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ru.gkalabin.diploma.Config;
import ru.gkalabin.diploma.gui.PlayFileHandler;
import ru.gkalabin.diploma.gui.UnitConversion;

import java.io.File;
import java.io.IOException;

/**
 * @author grigory.kalabin@gmail.com
 */
public class EqualizerDemo extends Application {

  private static final int PADDING = 10;
  private static final String EQUALIZER_CHECK_BOX_ID_FORMAT = "EqualizerCheckBox#%d";
  private static final String EQUALIZER_SLIDER_ID_FORMAT = "EqualizerSlider#%d";

  private TextField myInputFileField;
  private TextField myOutputFileField;

  private ToggleGroup myWindowLengthControl;
  private int myWindowLength;

  private final GridPane myEqualizerGrid = new GridPane();
  private final Slider myEqualizerSlider1 = getEqualizerSlider(1);
  private final Slider myEqualizerSlider2 = getEqualizerSlider(2);
  private final Slider myEqualizerSlider3 = getEqualizerSlider(3);
  private final Slider myEqualizerSlider4 = getEqualizerSlider(4);
  private final Slider myEqualizerSlider5 = getEqualizerSlider(5);
  private final Slider myEqualizerSlider6 = getEqualizerSlider(6);
  private final Slider myEqualizerSlider7 = getEqualizerSlider(7);
  private final Slider myEqualizerSlider8 = getEqualizerSlider(8);
  private final CheckBox myEqualizerCutOutCheckBox1 = getEqualizerCutOutCheckBox(1);
  private final CheckBox myEqualizerCutOutCheckBox2 = getEqualizerCutOutCheckBox(2);
  private final CheckBox myEqualizerCutOutCheckBox3 = getEqualizerCutOutCheckBox(3);
  private final CheckBox myEqualizerCutOutCheckBox4 = getEqualizerCutOutCheckBox(4);
  private final CheckBox myEqualizerCutOutCheckBox5 = getEqualizerCutOutCheckBox(5);
  private final CheckBox myEqualizerCutOutCheckBox6 = getEqualizerCutOutCheckBox(6);
  private final CheckBox myEqualizerCutOutCheckBox7 = getEqualizerCutOutCheckBox(7);
  private final CheckBox myEqualizerCutOutCheckBox8 = getEqualizerCutOutCheckBox(8);
  private final Label myEqualizerLabel1 = LabelBuilder.create().textOverrun(OverrunStyle.CLIP).build();
  private final Label myEqualizerLabel2 = LabelBuilder.create().textOverrun(OverrunStyle.CLIP).build();
  private final Label myEqualizerLabel3 = LabelBuilder.create().textOverrun(OverrunStyle.CLIP).build();
  private final Label myEqualizerLabel4 = LabelBuilder.create().textOverrun(OverrunStyle.CLIP).build();
  private final Label myEqualizerLabel5 = LabelBuilder.create().textOverrun(OverrunStyle.CLIP).build();
  private final Label myEqualizerLabel6 = LabelBuilder.create().textOverrun(OverrunStyle.CLIP).build();
  private final Label myEqualizerLabel7 = LabelBuilder.create().textOverrun(OverrunStyle.CLIP).build();
  private final Label myEqualizerLabel8 = LabelBuilder.create().textOverrun(OverrunStyle.CLIP).build();
  private final TextArea myLogTextArea = new TextArea();
  private final Button myRunButton = new Button("Start");
  private final Button myPlayResultButton = new Button("Play result");

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(final Stage primaryStage) {
    primaryStage.setTitle("Short-time Walsh transform demo");
    Text title = new Text("Short-time Walsh transform demo");
    title.setFont(Font.font("Tahoma", FontWeight.NORMAL, 32));

    Text ioTitle = new Text("I/O");
    title.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

    GridPane ioGrid = new GridPane();
    ioGrid.setHgap(PADDING);
    ioGrid.setVgap(PADDING);
    ioGrid.setAlignment(Pos.CENTER);
    ColumnConstraints labelsColumn = new ColumnConstraints();
    labelsColumn.setHalignment(HPos.RIGHT);
    ColumnConstraints inputFieldsColumn = new ColumnConstraints();
    inputFieldsColumn.setHgrow(Priority.ALWAYS);
    ioGrid.getColumnConstraints().addAll(labelsColumn, inputFieldsColumn);

    myInputFileField = new TextField();
    Button pickInputFile = new Button("Pick");
    pickInputFile.setOnAction(new EventHandler<ActionEvent>() {
      private File myInitialDir = new File(".");

      @Override
      public void handle(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(myInitialDir);
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Wav files (*.wav)", "*.wav");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showOpenDialog(primaryStage);
        if (file != null) {
          myInitialDir = file.getParentFile();
          myInputFileField.setText(file.getAbsolutePath());
          File outFile = new File(Config.OUT_DIR, file.getName());
          try {
            myOutputFileField.setText(outFile.getCanonicalPath());
          } catch (IOException e) {
            // should never happen
          }
        }
      }
    });
    Button playInputFile = new Button("Play");
    playInputFile.setOnAction(new PlayFileHandler(myInputFileField, playInputFile));
    ioGrid.add(new Label("Input file:"), 0, 0);
    ioGrid.add(myInputFileField, 1, 0);
    ioGrid.add(pickInputFile, 2, 0);
    ioGrid.add(playInputFile, 3, 0);

    myOutputFileField = new TextField();
    ioGrid.add(new Label("Output file:"), 0, 1);
    ioGrid.add(myOutputFileField, 1, 1);
    ioGrid.add(new Label(), 2, 1);
    ioGrid.add(new Label(), 3, 1);

    HBox windowLengthBox = new HBox();
    myWindowLengthControl = new ToggleGroup();
    EventHandler<ActionEvent> windowLengthChangeHandler = new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        myWindowLength = (int) myWindowLengthControl.getSelectedToggle().getUserData();
        setupEqualizerLabels();
      }
    };
    windowLengthBox.getChildren().addAll(
        new Label("Window length:    "),
        getWindowLengthRadioButton(128, windowLengthChangeHandler),
        getWindowLengthRadioButton(256, windowLengthChangeHandler),
        getWindowLengthRadioButton(512, windowLengthChangeHandler),
        getWindowLengthRadioButton(1024, windowLengthChangeHandler),
        getWindowLengthRadioButton(2048, windowLengthChangeHandler),
        getWindowLengthRadioButton(4096, windowLengthChangeHandler),
        getWindowLengthRadioButton(8192, windowLengthChangeHandler)
    );

    myEqualizerGrid.setHgap(PADDING * 10);
    myEqualizerGrid.setVgap(PADDING);
    myEqualizerGrid.setAlignment(Pos.CENTER);

    myEqualizerGrid.add(new Text("Frequency range"), 0, 0);
    myEqualizerGrid.add(myEqualizerLabel1, 1, 0);
    myEqualizerGrid.add(myEqualizerLabel2, 2, 0);
    myEqualizerGrid.add(myEqualizerLabel3, 3, 0);
    myEqualizerGrid.add(myEqualizerLabel4, 4, 0);
    myEqualizerGrid.add(myEqualizerLabel5, 5, 0);
    myEqualizerGrid.add(myEqualizerLabel6, 6, 0);
    myEqualizerGrid.add(myEqualizerLabel7, 7, 0);
    myEqualizerGrid.add(myEqualizerLabel8, 8, 0);

    myEqualizerGrid.add(new Text("Equalization coeff"), 0, 1);
    myEqualizerGrid.add(myEqualizerSlider1, 1, 1);
    myEqualizerGrid.add(myEqualizerSlider2, 2, 1);
    myEqualizerGrid.add(myEqualizerSlider3, 3, 1);
    myEqualizerGrid.add(myEqualizerSlider4, 4, 1);
    myEqualizerGrid.add(myEqualizerSlider5, 5, 1);
    myEqualizerGrid.add(myEqualizerSlider6, 6, 1);
    myEqualizerGrid.add(myEqualizerSlider7, 7, 1);
    myEqualizerGrid.add(myEqualizerSlider8, 8, 1);

    myEqualizerGrid.add(new Text("Cut the whole range"), 0, 2);
    myEqualizerGrid.add(myEqualizerCutOutCheckBox1, 1, 2);
    myEqualizerGrid.add(myEqualizerCutOutCheckBox2, 2, 2);
    myEqualizerGrid.add(myEqualizerCutOutCheckBox3, 3, 2);
    myEqualizerGrid.add(myEqualizerCutOutCheckBox4, 4, 2);
    myEqualizerGrid.add(myEqualizerCutOutCheckBox5, 5, 2);
    myEqualizerGrid.add(myEqualizerCutOutCheckBox6, 6, 2);
    myEqualizerGrid.add(myEqualizerCutOutCheckBox7, 7, 2);
    myEqualizerGrid.add(myEqualizerCutOutCheckBox8, 8, 2);

    HBox controlsBox = new HBox();
    controlsBox.setAlignment(Pos.CENTER_RIGHT);
    controlsBox.setSpacing(PADDING);
    myRunButton.setPrefSize(100, 40);
    myRunButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        myRunButton.setDisable(true);
        myPlayResultButton.setDisable(true);
        myLogTextArea.clear();

        EqualizerDemoRunner runner = new EqualizerDemoRunner(
            myInputFileField.getText(), myOutputFileField.getText(), myWindowLength, getEqualiserCoefficients(),
            new EqualizerDemoRunner.Logger() {
              private final StringBuilder myBuffer = new StringBuilder();

              @Override
              public void log(final String msg) {
                myBuffer.append(msg);
                myBuffer.append("\n\r");
              }

              @Override
              public void flush() {
                Platform.runLater(new Runnable() {
                  public void run() {
                    myLogTextArea.appendText(myBuffer.toString());
                  }
                });
              }
            }
        );
        new Thread(runner).start();

        myRunButton.setDisable(false);
        myPlayResultButton.setDisable(false);
      }
    });
    myPlayResultButton.setPrefSize(100, 40);
    myPlayResultButton.setDisable(true);
    myPlayResultButton.setOnAction(new PlayFileHandler(myOutputFileField, myPlayResultButton));
    controlsBox.getChildren().addAll(myRunButton, myPlayResultButton);

    myLogTextArea.setDisable(false);
    myLogTextArea.setEditable(false);
//    myLogTextArea.setPrefRowCount(Integer.MAX_VALUE);

    setupDefaults();
    StackPane root = new StackPane();
    VBox vBox = new VBox(PADDING);
    vBox.setPadding(new Insets(PADDING));
    VBox.setVgrow(myLogTextArea, Priority.ALWAYS);
    vBox.getChildren().addAll(
        title,
        new Separator(),
        ioTitle, ioGrid,
        new Separator(),
        windowLengthBox,
        new Separator(),
        myEqualizerGrid,
        new Separator(),
        myLogTextArea,
        controlsBox
    );
    root.getChildren().addAll(vBox);

    primaryStage.setScene(new Scene(root));
    primaryStage.show();
  }

  private double[] getEqualiserCoefficients() {
    double[] result = new double[8];
    result[0] = getEqualizerCoefficient(myEqualizerCutOutCheckBox1, myEqualizerSlider1);
    result[1] = getEqualizerCoefficient(myEqualizerCutOutCheckBox2, myEqualizerSlider2);
    result[2] = getEqualizerCoefficient(myEqualizerCutOutCheckBox3, myEqualizerSlider3);
    result[3] = getEqualizerCoefficient(myEqualizerCutOutCheckBox4, myEqualizerSlider4);
    result[4] = getEqualizerCoefficient(myEqualizerCutOutCheckBox5, myEqualizerSlider5);
    result[5] = getEqualizerCoefficient(myEqualizerCutOutCheckBox6, myEqualizerSlider6);
    result[6] = getEqualizerCoefficient(myEqualizerCutOutCheckBox7, myEqualizerSlider7);
    result[7] = getEqualizerCoefficient(myEqualizerCutOutCheckBox8, myEqualizerSlider8);
    return result;
  }

  private double getEqualizerCoefficient(CheckBox cutOutCheckBox, Slider slider) {
    return cutOutCheckBox.isSelected() ? 0d : UnitConversion.dB2Times(slider.getValue());
  }

  private void setupDefaults() {
    for (Toggle t : myWindowLengthControl.getToggles()) {
      if (t.getUserData().equals(Config.WINDOW_LENGTH)) {
        t.setSelected(true);
        myWindowLength = Config.WINDOW_LENGTH;
      }
    }
    if (myWindowLengthControl.getSelectedToggle() == null) {
      throw new RuntimeException("Config not valid!");
    }

    setupEqualizerLabels();

    try {
      myInputFileField.setText(Config.INPUT_FILE.getCanonicalPath());
      myOutputFileField.setText(new File(Config.OUT_DIR, Config.INPUT_FILE.getName()).getCanonicalPath());
    } catch (IOException e) {
      // TODO: log!
      e.printStackTrace();
    }
  }

  private void setupEqualizerLabels() {
    String labelTextFormat = "%d:%d";
    myEqualizerLabel1.setText(String.format(labelTextFormat, 0, myWindowLength / 8 - 1));
    myEqualizerLabel2.setText(String.format(labelTextFormat, myWindowLength / 8, myWindowLength * 2 / 8 - 1));
    myEqualizerLabel3.setText(String.format(labelTextFormat, myWindowLength * 2 / 8, myWindowLength * 3 / 8 - 1));
    myEqualizerLabel4.setText(String.format(labelTextFormat, myWindowLength * 3 / 8, myWindowLength * 4 / 8 - 1));
    myEqualizerLabel5.setText(String.format(labelTextFormat, myWindowLength * 4 / 8, myWindowLength * 5 / 8 - 1));
    myEqualizerLabel6.setText(String.format(labelTextFormat, myWindowLength * 5 / 8, myWindowLength * 6 / 8 - 1));
    myEqualizerLabel7.setText(String.format(labelTextFormat, myWindowLength * 6 / 8, myWindowLength * 7 / 8 - 1));
    myEqualizerLabel8.setText(String.format(labelTextFormat, myWindowLength * 7 / 8, myWindowLength * 8 / 8 - 1));
  }

  private RadioButton getWindowLengthRadioButton(int value, EventHandler<ActionEvent> windowLengthChangeHandler) {
    return RadioButtonBuilder.create()
        .text(value + Strings.repeat(" ", 10))
        .userData(value)
        .onAction(windowLengthChangeHandler)
        .toggleGroup(myWindowLengthControl)
        .build();
  }

  /**
   * @param idx ***one-based***
   */
  private CheckBox getEqualizerCutOutCheckBox(int idx) {
    final CheckBox cb = CheckBoxBuilder.create()
        .allowIndeterminate(false)
        .selected(false)
        .text("Cut Out")
        .id(String.format(EQUALIZER_CHECK_BOX_ID_FORMAT, idx))
        .userData(idx)
        .build();
    cb.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        int idx = (int) cb.getUserData();
        Slider slider = (Slider) myEqualizerGrid.lookup("#" + String.format(EQUALIZER_SLIDER_ID_FORMAT, idx));
        slider.setDisable(cb.isSelected());
      }
    });
    return cb;
  }

  /**
   * @param idx ***one-based***
   */
  private Slider getEqualizerSlider(int idx) {
    return SliderBuilder.create()
        .orientation(Orientation.VERTICAL)
        .min(-10d)
        .max(10d)
        .showTickLabels(true)
        .showTickMarks(true)
        .majorTickUnit(5d)
        .minorTickCount(0)
        .id(String.format(EQUALIZER_SLIDER_ID_FORMAT, idx))
        .userData(idx)
        .build();
  }
}
