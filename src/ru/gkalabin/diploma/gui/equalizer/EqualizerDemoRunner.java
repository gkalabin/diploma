package ru.gkalabin.diploma.gui.equalizer;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import javafx.concurrent.Task;
import ru.gkalabin.diploma.Config;
import ru.gkalabin.diploma.filter.Filter;
import ru.gkalabin.diploma.filter.MediumPassFilter;
import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.model.WalshWindowedSpectrum;
import ru.gkalabin.diploma.codec.wav.WavReader;
import ru.gkalabin.diploma.codec.wav.WavWriter;
import ru.gkalabin.diploma.window.RectangularWindow;
import ru.gkalabin.diploma.window.Window;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * @author grigory.kalabin@gmail.com
 */
public class EqualizerDemoRunner extends Task {
  public interface Logger {
    void log(String msg);
    void flush();
  }

  private final int myWindowLength;
  // in times, not in dB!!
  private final double[] myFrequencyCoefficients;
  private final Logger myLogger;
  private final File myInputFile;
  private final File myOutputFile;

  public EqualizerDemoRunner(String inputFile, String outFile, int windowLength, double[] frequencyCoefficients, Logger logger) {
    myWindowLength = windowLength;
    myFrequencyCoefficients = frequencyCoefficients;
    myLogger = logger;
    myInputFile = new File(inputFile);
    myOutputFile = new File(outFile);
  }

  @Override
  public Void call() {
    try {
      doRun();
    } catch (Exception e) {
      myLogger.log("Unhandled exception: " + e.getMessage());
      e.printStackTrace();
    }
    myLogger.flush();
    return null;
  }

  private void doRun() throws IOException, UnsupportedAudioFileException {
    log("Start processing %s", myInputFile.getCanonicalPath());
    log("Window length = " + myWindowLength);
    long t0 = System.currentTimeMillis();
    WavReader reader = new WavReader(myInputFile);
    Signal signal = reader.getSignal();
    long t1 = System.currentTimeMillis();
    log("Signal reading (length = %d) took %d ms", signal.getLength(), t1 - t0);
    Window window = new RectangularWindow(myWindowLength, Config.WINDOW_SHIFT_NO_OVERLAP);

    List<Filter> filters = Lists.newArrayList();
    int rangesCount = myFrequencyCoefficients.length;
    for (int i = 0; i < rangesCount; i++) {
      int minFrequency = myWindowLength * i / rangesCount;
      int maxFrequency = myWindowLength * (i + 1) / rangesCount;
      MediumPassFilter filter = new MediumPassFilter(minFrequency, maxFrequency, myFrequencyCoefficients[i]);
      filters.add(filter);
      log("Adding filter: %s", filter);
    }

    long t2 = System.currentTimeMillis();
    log("Creating filters took %d ms", t2 - t1);
    logSeparator();

    Iterator<WalshWindowedSignal> iterator = reader.getSignal().getIterator(window);
    List<WalshWindowedSignal> windowedSignals = Lists.newArrayList();

    while (iterator.hasNext()) {
      long tI = System.currentTimeMillis();
      WalshWindowedSignal windowedSignal = iterator.next();
      log("Processing window from %d (inclusive) to %d (exclusive)", windowedSignal.getLeft(), windowedSignal.getRight());
      long tII = System.currentTimeMillis();
      log("Signal obtained for %d ms", tII - tI);

      WalshWindowedSpectrum spectrum = windowedSignal.toSpectrum();
      long tIII = System.currentTimeMillis();
      log("Spectrum obtained for %d ms", tIII - tII);

      spectrum.applyFilters(filters);
      long tIV = System.currentTimeMillis();
      log("Filtered for %d ms", tIV - tIII);

      WalshWindowedSignal revertedSignal = spectrum.toSignal();
      long tV = System.currentTimeMillis();
      log("Windowed signal reverted for %d ms", tV - tIV);
      windowedSignals.add(revertedSignal);
      logSeparator();
    }

    long t3 = System.currentTimeMillis();
    log("Whole signal processing took %d ms", t3 - t2);

    Signal revSignal = Signal.fromWindowedSignals(windowedSignals, reader.getSigLength());
    long t4 = System.currentTimeMillis();
    log("Prepare signal for writing took %d ms", t4 - t3);
    WavWriter writer = new WavWriter(myOutputFile, reader);
    writer.write(revSignal);
    long t5 = System.currentTimeMillis();
    log("Writing signal took %d ms", t5 - t4);
    log("Finished for %d ms", t5 - t0);
  }


  private void logSeparator() {
    myLogger.log(Strings.repeat("-", 40));
  }

  private void log(String msg) {
    myLogger.log(msg);
  }

  private void log(String msgFormat, Object... args) {
    myLogger.log(String.format(msgFormat, args));
  }
}
