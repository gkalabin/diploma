package ru.gkalabin.diploma.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

/**
 * @author grigory.kalabin@gmail.com
 */
public class PlayFileHandler implements EventHandler<ActionEvent> {
  private boolean isPlaying;
  private final TextField myFileField;
  private final Button myActionButton;
  private final String myButtonText;
  private MediaPlayer myMediaPlayer;

  public PlayFileHandler(TextField fileField, Button actionButton) {
    myFileField = fileField;
    myActionButton = actionButton;
    myButtonText = myActionButton.getText();
  }

  @Override
  public void handle(ActionEvent actionEvent) {
    if (!isPlaying) {
      run();
    } else {
      stop();
    }
  }

  private void run() {
    Media media = new Media(new File(myFileField.getText()).toURI().toString());
    myMediaPlayer = new MediaPlayer(media);
    myMediaPlayer.setOnEndOfMedia(new Runnable() {
      @Override
      public void run() {
        stop();
      }
    });
    myMediaPlayer.play();
    isPlaying = true;
    myActionButton.setText("Stop");
  }

  private void stop() {
    myActionButton.setText(myButtonText);
    if (myMediaPlayer.getStatus() != MediaPlayer.Status.STOPPED) {
      myMediaPlayer.stop();
    }
    isPlaying = false;
    myMediaPlayer = null;
  }
}
