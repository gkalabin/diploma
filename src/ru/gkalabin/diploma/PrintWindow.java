package ru.gkalabin.diploma;

import ru.gkalabin.diploma.window.HammingWindow;
import ru.gkalabin.diploma.window.Window;
import ru.gkalabin.diploma.window.WindowFrame;

import java.util.Iterator;

/**
 * @author grigory.kalabin@gmail.com
 */
public class PrintWindow {
  public static void main(String[] args) {
    Window window = new HammingWindow(Config.WINDOW_LENGTH, .25d);
    Iterator<WindowFrame> frameIterator = window.getFrameIterator(Config.WINDOW_LENGTH);
    while (frameIterator.hasNext()) {
      WindowFrame frame = frameIterator.next();
      System.out.println(frame.toString(Config.WINDOW_LENGTH));
    }
  }
}
