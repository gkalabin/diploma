package ru.gkalabin.diploma;

import ru.gkalabin.diploma.util.Util;
import ru.gkalabin.diploma.walsh.Walsh;

/**
 * @author grigory.kalabin@gmail.com
 */
public class PrintWalshFunction {
  public static void main(String[] args) {
    int N = 4;
    int S = Util.intLog2(N);
    Walsh walsh = new Walsh(S);

    System.out.printf("Not ordered:\n%s\n\n", walsh.toString(false));
    System.out.printf("Ordered:\n%s", walsh.toString(true));
  }
}
