package ru.gkalabin.diploma.walsh;

import java.util.Arrays;

/**
 * @author grigory.kalabin@gmail.com
 */
public class FWT {
  private static double[] _order(double[] value, int n, int s) {
    double[] result = new double[n];
    for (int i = 0; i < n; i++) {
      result[Walsh.rev(s, i)] = value[i];
    }
    return result;
  }

  static double[] slowWT(double[] signal, int n, int s) {
    double[] X = new double[n];
    Arrays.fill(X, 0d);
    Walsh w = new Walsh(s);
    for (int k = 0; k < n; k++) {
      for (int j = 0; j < n; j++) {
        double x_j = signal[j];
        double v_j = w.v(k).apply(j);
        double summand = x_j * v_j;
        X[k] = X[k] + summand;
      }
      X[k] /= n;
    }
    return _order(X, n, s);
  }

  public static double[] fastWT(double[] signal, int n, int s) {
    double[] xPrev = new double[n];
    System.arraycopy(signal, 0, xPrev, 0, n);
    double[] xCurr = new double[n];
    int N_nu = n;
    int delta_nu = 1;
    for (int nu = 1; nu <= s; nu++) {
      N_nu /= 2;
      int next_delta_nu = delta_nu * 2;
      for (int l = 0; l < delta_nu; l++) {
        for (int p = 0; p < N_nu; p++) {
          int leftIdx = l + p * next_delta_nu;
          int idxFirstOperand = l + 2 * p * delta_nu;
          int idxSecondOperand = l + (2 * p + 1) * delta_nu;
          xCurr[leftIdx] = .5d * (xPrev[idxFirstOperand] + xPrev[idxSecondOperand]);
          xCurr[leftIdx + delta_nu] = .5d * (xPrev[idxFirstOperand] - xPrev[idxSecondOperand]);
        }
      }
      delta_nu *= 2;
      System.arraycopy(xCurr, 0, xPrev, 0, n);
    }
    return _order(xCurr, n, s);
  }

  public static double[] fastRevWT(double[] spectrum, int n, int s) {
    double[] xPrev = new double[n];
    double[] xCurr = new double[n];
    System.arraycopy(spectrum, 0, xCurr, 0, n);
    // N_s
    int N_nu = 1;
    // delta_s
    int delta_nu = n / 2;
    for (int nu = s; nu >= 1; nu--) {
      int next_delta_nu = delta_nu * 2;
      for (int l = 0; l < delta_nu; l++) {
        for (int p = 0; p < N_nu; p++) {
          int leftIdx = l + 2 * p * delta_nu;
          int idxFirstOperand = l + p * next_delta_nu;
          int idxSecondOperand = l + p * next_delta_nu + delta_nu;
          xPrev[leftIdx] = xCurr[idxFirstOperand] + xCurr[idxSecondOperand];
          xPrev[leftIdx + delta_nu] = xCurr[idxFirstOperand] - xCurr[idxSecondOperand];
        }
      }
      N_nu *= 2;
      delta_nu /= 2;
      System.arraycopy(xPrev, 0, xCurr, 0, n);
    }
    return _order(xPrev, n, s);
  }
}
