package ru.gkalabin.diploma;

import com.google.common.collect.Lists;
import ru.gkalabin.diploma.filter.IdFilter;
import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.model.WalshWindowedSpectrum;
import ru.gkalabin.diploma.util.GraphUtil;
import ru.gkalabin.diploma.codec.wav.WavReader;
import ru.gkalabin.diploma.codec.wav.WavWriter;
import ru.gkalabin.diploma.window.RectangularWindow;
import ru.gkalabin.diploma.window.Window;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Read, window walsh, revert and write
 *
 * @author grigory.kalabin@gmail.com
 */
public class ShowSigDetails {
  public static void main(String[] args) throws IOException, UnsupportedAudioFileException, InterruptedException {
    long t0 = System.currentTimeMillis();
    File file = Config.INPUT_FILE;
    WavReader reader = new WavReader(file);
    Signal signal = reader.getSignal();
    System.out.println("Signal read: ");
    GraphUtil.doubles2dat(signal.getValue(), file.getName() + "_src");

    Window window = new RectangularWindow(Config.WINDOW_LENGTH, Config.WINDOW_SHIFT_NO_OVERLAP);
    Iterator<WalshWindowedSignal> iterator = signal.getIterator(window);
    List<WalshWindowedSignal> windowedSignals = Lists.newArrayList();

    int idx = 0;
    while (iterator.hasNext()) {
      WalshWindowedSignal windowedSignal = iterator.next();
      GraphUtil.samples2dat(windowedSignal, file.getName() + "_window_" + idx);

      WalshWindowedSpectrum spectrum = windowedSignal.toSpectrum();
      spectrum.applyFilter(new IdFilter());
      GraphUtil.samples2dat(spectrum, file.getName() + "_spectrum_" + idx);

      WalshWindowedSignal revertedSignal = spectrum.toSignal();
      windowedSignals.add(revertedSignal);
      GraphUtil.samples2dat(spectrum, file.getName() + "_reverted_" + idx);
      idx++;
    }
    System.out.println("Processed. Got " + windowedSignals.size() + " windowed signals");

    Signal revSignal = Signal.fromWindowedSignals(windowedSignals, reader.getSigLength());
    System.out.println("Copied to output");

    WavWriter writer = new WavWriter(Config.OUTPUT_FILE, reader);
    writer.write(revSignal);
    System.out.printf("done: %d ms", System.currentTimeMillis() - t0);

    GraphUtil.replot();
  }
}
