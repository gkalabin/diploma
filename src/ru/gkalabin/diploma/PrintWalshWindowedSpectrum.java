package ru.gkalabin.diploma;

import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.model.WalshWindowedSpectrum;
import ru.gkalabin.diploma.util.Util;
import ru.gkalabin.diploma.walsh.Walsh;
import ru.gkalabin.diploma.window.HammingWindow;
import ru.gkalabin.diploma.window.Window;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * @author grigory.kalabin@gmail.com
 */
public class PrintWalshWindowedSpectrum {
  public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
    double[] samples = {1, 2, 3, 4, 5, 6, 7, 8};
    int windowLength = 4;
    Window window = new HammingWindow(windowLength, .25d);

    Signal signal = new Signal(samples);
    System.out.printf("Signal:\n%s\n\n", signal);

    System.out.printf("Window:\n%s\n\n", window.getFrame(2).toString(samples.length));

    WalshWindowedSignal windowedSignal = window.getFrame(2).apply(signal);
    System.out.printf("Windowed signal:\n%s\n\n", windowedSignal);

    Walsh walsh = new Walsh(Util.intLog2(windowLength));
    System.out.printf("Walsh:\n%s\n", walsh.toString(false));


    WalshWindowedSpectrum windowedSpectrum = windowedSignal.toSpectrum();
    System.out.printf("Windowed spectrum:\n%s", windowedSpectrum);
  }
}
