package ru.gkalabin.diploma;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.util.DoubleArrayDiff;
import ru.gkalabin.diploma.util.GraphUtil;
import ru.gkalabin.diploma.window.HammingWindow;
import ru.gkalabin.diploma.window.Window;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author grigory.kalabin@gmail.com
 */
public class WindowShiftDemo {

  public static final int SIG_LENGTH = 4000;

  public static void main(String[] args) throws IOException, InterruptedException {
    double[] samples = new double[SIG_LENGTH];
    Arrays.fill(samples, 1d);
    Signal signal = new Signal(samples);

    Map<DoubleArrayDiff, Double> diffs = Maps.newHashMap();
    for (double shift = .01d; shift <= 1d; shift += .01d) {
      diffs.put(processShift(signal, shift, false), shift);
    }

    DoubleArrayDiff bestMax = null;
    DoubleArrayDiff bestAvg = null;
    for (DoubleArrayDiff diff : diffs.keySet()) {
      if (bestMax == null) {
        bestMax = diff;
      }
      if (bestAvg == null) {
        bestAvg = diff;
      }
      if (bestMax.getMaxDiff() > diff.getMaxDiff()) {
        bestMax = diff;
      }
      if (bestAvg.getAvgDiff() > diff.getAvgDiff()) {
        bestAvg = diff;
      }
    }

    Double bestMaxShift = diffs.get(bestMax);
    System.out.printf("Best max at %.2f: %s\n", bestMaxShift, bestMax);
    processShift(signal, bestMaxShift, true);
    Double bestAvgShift = diffs.get(bestAvg);
    System.out.printf("Best avg at %.2f: %s\n", bestAvgShift, bestAvg);
    processShift(signal, bestAvgShift, true);

    GraphUtil.doubles2dat(signal.getValue(), "SrcSignal");
    GraphUtil.replot();
  }

  private static DoubleArrayDiff processShift(Signal signal, double shift, boolean plot) throws IOException {
    Window window = new HammingWindow(Config.WINDOW_LENGTH, shift);

    Iterator<WalshWindowedSignal> iterator = signal.getIterator(window);
    List<WalshWindowedSignal> windowedSignals = Lists.newArrayList();
    while (iterator.hasNext()) {
      windowedSignals.add(iterator.next());
    }

    Signal restoredSignal = Signal.fromWindowedSignals(windowedSignals, signal.getLength());
    if (plot) {
      GraphUtil.doubles2dat(restoredSignal.getValue(), String.format("RestoredSignal_%.2f", shift));
    }
    return DoubleArrayDiff.getDiff(signal.getValue(), restoredSignal.getValue());
  }

}
