package ru.gkalabin.diploma;

import com.google.common.collect.Lists;
import ru.gkalabin.diploma.filter.Filter;
import ru.gkalabin.diploma.filter.LowPassFilter;
import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.model.WalshWindowedSpectrum;
import ru.gkalabin.diploma.codec.wav.WavReader;
import ru.gkalabin.diploma.codec.wav.WavWriter;
import ru.gkalabin.diploma.window.RectangularWindow;
import ru.gkalabin.diploma.window.Window;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Read, window walsh, revert and write
 *
 * @author grigory.kalabin@gmail.com
 */
public class ExtremeFilterDemo {
  public static void main(String[] args) throws IOException, UnsupportedAudioFileException, InterruptedException {
    process(new File("./samples/vivaldi.wav"));
    process(new File("./samples/metallica.wav"));
    process(new File("./samples/chopin.wav"));
  }

  private static void process(File file) throws IOException, UnsupportedAudioFileException, InterruptedException {
    WavReader reader = new WavReader(file);
    reader.getSignal();
    String baseFileName = file.getName().substring(0, file.getName().length() - 4);

    int currFrequency = Config.WINDOW_LENGTH / 2;

    while (currFrequency > 0) {
      Filter lpf = new LowPassFilter(currFrequency);
      System.out.println("0.." + currFrequency + ": " + processFilters(reader, Lists.newArrayList(lpf),
          baseFileName + "_[0:" + currFrequency + "]") + " ms");
      currFrequency /= 2;
    }
  }

  private static long processFilters(WavReader reader, List<Filter> filters, String outFileName) throws IOException, UnsupportedAudioFileException, InterruptedException {
    long t0 = System.currentTimeMillis();
    Window window = new RectangularWindow(Config.WINDOW_LENGTH, Config.WINDOW_SHIFT_NO_OVERLAP);
    Iterator<WalshWindowedSignal> iterator = reader.getSignal().getIterator(window);
    List<WalshWindowedSignal> windowedSignals = Lists.newArrayList();

    while (iterator.hasNext()) {
      WalshWindowedSignal windowedSignal = iterator.next();

      WalshWindowedSpectrum spectrum = windowedSignal.toSpectrum();
      spectrum.applyFilters(filters);

      WalshWindowedSignal revertedSignal = spectrum.toSignal();
      windowedSignals.add(revertedSignal);
    }

    Signal revSignal = Signal.fromWindowedSignals(windowedSignals, reader.getSigLength());
    WavWriter writer = new WavWriter(new File("out/" + outFileName + ".wav"), reader);
    writer.write(revSignal);
    return System.currentTimeMillis() - t0;
  }
}
