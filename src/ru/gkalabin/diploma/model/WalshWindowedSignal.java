package ru.gkalabin.diploma.model;

import ru.gkalabin.diploma.util.Util;
import ru.gkalabin.diploma.walsh.FWT;

/**
 * @author grigory.kalabin@gmail.com
 */
public class WalshWindowedSignal extends WindowedSamplesWrapper {
  public WalshWindowedSignal(double[] samples, int left, int right) {
    super(samples, left, right);
  }

  public WalshWindowedSpectrum toSpectrum() {
    double[] samples = getValue();
    int n = samples.length;
    int s = Util.intLog2(n);
    double[] value = FWT.fastWT(samples, n, s);
    return new WalshWindowedSpectrum(value, getLeft(), getRight());
  }
}
