package ru.gkalabin.diploma.model;

import com.google.common.base.Preconditions;

/**
 * @author grigory.kalabin@gmail.com
 */
public abstract class WindowedSamplesWrapper {

  private final double[] mySamples;
  private final int myLeft;
  private final int myRight;

  public WindowedSamplesWrapper(double[] samples, int left, int right) {
    Preconditions.checkArgument(samples.length == right - left);
    mySamples = samples;
    myLeft = left;
    myRight = right;
  }

  public double get(int i) {
    if (i < myLeft || i >= myRight) {
      return 0d;
    }
    return mySamples[i - myLeft];
  }

  public int getLeft() {
    return myLeft;
  }

  public int getRight() {
    return myRight;
  }

  public double[] getValue() {
    return mySamples;
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    for (double mySample : mySamples) {
      result.append(String.format("%.2f    ", mySample));
    }
    return result.toString();
  }
}
