package ru.gkalabin.diploma.model;

import com.google.common.base.Preconditions;
import ru.gkalabin.diploma.util.Util;
import ru.gkalabin.diploma.window.Window;
import ru.gkalabin.diploma.window.WindowFrame;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author grigory.kalabin@gmail.com
 */
public class Signal {
  private final double[] myValue;

  public Signal(double[] value) {
    myValue = value;
  }

  public double get(int i) {
    return i < myValue.length ? myValue[i] : 0d;
  }

  public double[] get(int left, int right) {
    Preconditions.checkArgument(left < right);
    if (right <= myValue.length) {
      return Arrays.copyOfRange(myValue, left, right);
    }
    double[] result = new double[right - left];
    Arrays.fill(result, 0d);
    System.arraycopy(myValue, left, result, 0, myValue.length - left);
    return result;
  }

  public double[] getValue() {
    return myValue;
  }

  public int getLength() {
    return myValue.length;
  }

  public Iterator<WalshWindowedSignal> getIterator(Window window) {
    return new WalshSignalIterator(this, window);
  }

  public static Signal fromWindowedSignals(List<WalshWindowedSignal> signals, int length) {
    int samplesLength = 0;
    for (WalshWindowedSignal signal : signals) {
      samplesLength = Math.max(samplesLength, signal.getRight());
    }

    double[] samples = new double[samplesLength];
    Arrays.fill(samples, 0d);
    for (WalshWindowedSignal signal : signals) {
      // TODO: window overlap
      System.arraycopy(signal.getValue(), 0, samples, signal.getLeft(), signal.getRight() - signal.getLeft());
    }
    return new Signal(Util.trimArray(samples, length));
  }

  private static class WalshSignalIterator implements Iterator<WalshWindowedSignal> {
    private final Iterator<WindowFrame> myFrameIterator;
    private final Signal mySignal;

    private WalshSignalIterator(Signal signal, Window window) {
      mySignal = signal;
      myFrameIterator = window.getFrameIterator(mySignal.getLength());
    }

    @Override
    public boolean hasNext() {
      return myFrameIterator.hasNext();
    }

    @Override
    public WalshWindowedSignal next() {
      WindowFrame frame = myFrameIterator.next();
      return frame.apply(mySignal);
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }
}
