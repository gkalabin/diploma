package ru.gkalabin.diploma.model;

import ru.gkalabin.diploma.util.Util;
import ru.gkalabin.diploma.filter.Filter;
import ru.gkalabin.diploma.walsh.FWT;

import java.util.List;

/**
 * @author grigory.kalabin@gmail.com
 */
public class WalshWindowedSpectrum extends WindowedSamplesWrapper {

  public WalshWindowedSpectrum(double[] samples, int left, int right) {
    super(samples, left, right);
  }

  public WalshWindowedSignal toSignal() {
    double[] samples = getValue();
    int n = samples.length;
    int s = Util.intLog2(n);
    double[] value = FWT.fastRevWT(samples, n, s);
    return new WalshWindowedSignal(value, getLeft(), getRight());
  }

  public void applyFilter(Filter filter) {
    double[] samples = getValue();
    for (int i = 0; i < samples.length; i++) {
      samples[i] = filter.apply(i, samples[i]);
    }
  }

  public void applyFilters(List<Filter> filters) {
    for (Filter filter : filters) {
      applyFilter(filter);
    }
  }
}
