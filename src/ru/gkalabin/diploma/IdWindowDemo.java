package ru.gkalabin.diploma;

import com.google.common.collect.Lists;
import ru.gkalabin.diploma.model.Signal;
import ru.gkalabin.diploma.model.WalshWindowedSignal;
import ru.gkalabin.diploma.model.WalshWindowedSpectrum;
import ru.gkalabin.diploma.codec.wav.WavReader;
import ru.gkalabin.diploma.codec.wav.WavWriter;
import ru.gkalabin.diploma.window.RectangularWindow;
import ru.gkalabin.diploma.window.Window;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Read, window walsh, revert and write
 *
 * @author grigory.kalabin@gmail.com
 */
public class IdWindowDemo {
  public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
    long t0 = System.currentTimeMillis();
    WavReader reader = new WavReader(Config.INPUT_FILE);
    Signal signal = reader.getSignal();

    Window window = new RectangularWindow(Config.WINDOW_LENGTH, Config.WINDOW_SHIFT_NO_OVERLAP);

    Iterator<WalshWindowedSignal> iterator = signal.getIterator(window);
    List<WalshWindowedSignal> windowedSignals = Lists.newArrayList();

    while (iterator.hasNext()) {
      WalshWindowedSpectrum spectrum = iterator.next().toSpectrum();
      windowedSignals.add(spectrum.toSignal());
    }
    System.out.println("Processed. Got " + windowedSignals.size() + " windowed signals");

    Signal revSignal = Signal.fromWindowedSignals(windowedSignals, reader.getSigLength());
    System.out.println("Copied to output");

    WavWriter writer = new WavWriter(Config.OUTPUT_FILE, reader);
    writer.write(revSignal);
    System.out.printf("done: %d ms", System.currentTimeMillis() - t0);
  }
}
