package ru.gkalabin.diploma.util;

import com.google.common.base.Preconditions;
import ru.gkalabin.diploma.Config;

/**
* @author grigory.kalabin@gmail.com
*/
public class DoubleArrayDiff {
  private final double myMaxDiff;
  private final double mySumDiff;
  private final double myAvgDiff;
  private final int myDiffCount;

  DoubleArrayDiff(double maxDiff, double sumDiff, int diffCount) {
    myMaxDiff = maxDiff;
    mySumDiff = sumDiff;
    myDiffCount = diffCount;
    myAvgDiff = mySumDiff / (double) diffCount;
  }

  public static DoubleArrayDiff getDiff(double[] one, double[] other) {
    Preconditions.checkArgument(one.length == other.length);
    int diffCount = 0;
    double maxDiff = Double.MIN_VALUE;
    double diffSum = 0d;
    for (int i = 0, n = one.length; i < n; i++) {
      double d1 = one[i];
      double d2 = other[i];
      double diff = Math.abs(d1 - d2);
      if (diff > Config.DOUBLE_DELTA) {
        diffCount++;
        diffSum += diff;
        if (maxDiff < diff) {
          maxDiff = diff;
        }
      }
    }
    return new DoubleArrayDiff(maxDiff, diffSum, diffCount);
  }

  public double getMaxDiff() {
    return myMaxDiff;
  }

  public double getAvgDiff() {
    return myAvgDiff;
  }

  @Override
  public String toString() {
    return
        String.format("MaxDiff=%s, SumDiff=%s, AvgDiff=%s, DiffCount=%d",
            myMaxDiff, mySumDiff, myAvgDiff, myDiffCount);
  }
}
