package ru.gkalabin.diploma.util;

import java.util.Arrays;

/**
 * @author grigory.kalabin@gmail.com
 */
public class Util {
  private static final double LOG2 = Math.log(2);

  private Util() {
  }

  public static double lg2(double x) {
    return Math.log(x) / LOG2;
  }

  public static int intLog2(int N) {
    return (int) Math.ceil(lg2(N));
  }

  public static int padN(int N) {
    int s = intLog2(N);
    return int2pow(s);
  }

  /**
   * 2**s
   */
  public static int int2pow(int s) {
    return (int) Math.pow(2, s);
  }

  public static double[] padArray(double[] src) {
    int srcN = src.length;
    int padN = padN(srcN);
    if (srcN == padN) {
      return src;
    }
    double[] result = new double[padN];
    System.arraycopy(src, 0, result, 0, srcN);
    Arrays.fill(result, srcN, padN, 0d);
    return result;
  }

  public static double[] trimArray(double[] src, int length) {
    double[] result = new double[length];
    System.arraycopy(src, 0, result, 0, length);
    return result;
  }
}
