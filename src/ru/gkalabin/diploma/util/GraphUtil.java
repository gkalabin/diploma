package ru.gkalabin.diploma.util;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.common.io.Files;
import ru.gkalabin.diploma.Config;
import ru.gkalabin.diploma.model.WindowedSamplesWrapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author grigory.kalabin@gmail.com
 */
public class GraphUtil {

  private GraphUtil() {
  }

  public static void doubles2dat(double[] doubles, String name) throws IOException {
    File out = new File(Config.GRAPH_DIR, String.format(Config.DAT_FILE_FORMAT, name));
    try (BufferedWriter writer = Files.newWriter(out, Charsets.UTF_8)) {
      for (double d : doubles) {
        writer.write(d + "\n");
      }
      writer.flush();
    }
  }

  public static void samples2dat(WindowedSamplesWrapper samplesWrapper, String name) throws IOException {
    doubles2dat(samplesWrapper.getValue(), name);
  }

  public static void replot() throws IOException, InterruptedException {
    ProcessBuilder processBuilder = new ProcessBuilder()
        .command("/bin/bash", "-c", "./replot.sh " + Config.GRAPH_DIR.getCanonicalPath())
        .directory(new File("bin"))
        .redirectErrorStream(true);
    Process process = processBuilder.start();
    process.waitFor();
    try (InputStreamReader isr = new InputStreamReader(process.getInputStream(), Charsets.UTF_8)) {
      String output = CharStreams.toString(isr);
      if (Config.DEBUG) {
        System.out.printf("Replotting: %s%n", output);
      }
    }
  }

}
