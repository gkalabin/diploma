package ru.gkalabin.diploma.util;

import com.google.common.base.Preconditions;

/**
 * @author grigory.kalabin@gmail.com
 */
public class ByteArrayDiff {
  private final int myMaxDiff;
  private final int mySumDiff;
  private final double myAvgDiff;
  private final int myDiffCount;

  ByteArrayDiff(int maxDiff, int sumDiff, int diffCount) {
    myMaxDiff = maxDiff;
    mySumDiff = sumDiff;
    myDiffCount = diffCount;
    myAvgDiff = diffCount == 0 ? Double.NaN : mySumDiff / diffCount;
  }

  public static ByteArrayDiff getDiff(byte[] one, byte[] other) {
    Preconditions.checkArgument(one.length == other.length);
    int diffCount = 0;
    int maxDiff = Integer.MIN_VALUE;
    int diffSum = 0;
    for (int i = 0, n = one.length; i < n; i++) {
      byte d1 = one[i];
      byte d2 = other[i];
      int diff = Math.abs(d1 - d2);
      if (diff != 0) {
        diffCount++;
        diffSum += diff;
        if (maxDiff < diff) {
          maxDiff = diff;
        }
      }
    }
    return new ByteArrayDiff(maxDiff, diffSum, diffCount);
  }

  public int getMaxDiff() {
    return myMaxDiff;
  }

  public double getAvgDiff() {
    return myAvgDiff;
  }

  @Override
  public String toString() {
    return
        String.format("MaxDiff=%s, SumDiff=%s, AvgDiff=%s, DiffCount=%d",
            myMaxDiff, mySumDiff, myAvgDiff, myDiffCount);
  }
}
