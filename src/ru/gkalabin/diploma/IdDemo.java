package ru.gkalabin.diploma;

import ru.gkalabin.diploma.codec.wav.WavReader;
import ru.gkalabin.diploma.codec.wav.WavWriter;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;

/**
 * Read and write
 *
 * @author grigory.kalabin@gmail.com
 */
public class IdDemo {
  public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
    long t0 = System.currentTimeMillis();
    WavReader reader = new WavReader(Config.INPUT_FILE);
    WavWriter writer = new WavWriter(new File(Config.OUT_DIR, Config.INPUT_FILE.getName()), reader);
    writer.write(reader.getSignal());
    System.out.printf("done: %d ms", System.currentTimeMillis() - t0);
  }
}
